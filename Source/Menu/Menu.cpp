/*
Copyright (C) 2003, 2010 - Wolfire Games
Copyright (C) 2010-2017 - Lugaru contributors (see AUTHORS file)

This file is part of Lugaru.

Lugaru is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Lugaru is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lugaru.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Menu/Menu.hpp"
#include "Audio/AudioManager.hpp"
#include "Graphic/gamegl.hpp"
#include "Level/Campaign.hpp"
#include "User/Settings.hpp"
#include "Utils/Input.hpp"
#include "Version.hpp"

// Should not be needed, Menu should call methods from other classes to launch maps and challenges and so on
#include "Level/Awards.hpp"

#include "Menu.hpp"
#include <set>
#include <string>
#include <vector>

using namespace Game;

extern float multiplier;
extern std::set<std::pair<int, int>> resolutions;
extern int mainmenu;
extern std::vector<CampaignLevel> campaignlevels;
extern float musicvolume[4];
extern float oldmusicvolume[4];
extern bool stillloading;
extern bool visibleloading;
extern int whichchoice;
extern TrackID leveltheme;

extern void toggleFullscreen();

int entername = 0;
int changeusername = 0;
int newdifficulty = 0;
std::string newusername = "";
unsigned newuserselected = 0;
float newuserblinkdelay = 0;
bool newuserblink = false;

std::vector<MenuItem> Menu::items;

MenuItem::MenuItem(MenuItemType _type, int _id, const string& _text, Texture _texture, int _x, int _y, int _w, int _h, float _r, float _g, float _b, float _linestartsize, float _lineendsize)
    : type(_type)
    , id(_id)
    , text(_text)
    , texture(_texture)
    , x(_x)
    , y(_y)
    , w(_w)
    , h(_h)
    , r(_r)
    , g(_g)
    , b(_b)
    , effectfade(0)
    , linestartsize(_linestartsize)
    , lineendsize(_lineendsize)
{
    if (type == MenuItem::BUTTON) {
        if (w == -1) {
            w = text.length() * 10;
        }
        if (h == -1) {
            h = 20;
        }
    }
}

void Menu::clearMenu()
{
    items.clear();
}

void Menu::addLabel(int id, const string& text, int x, int y, float r, float g, float b)
{
    items.emplace_back(MenuItem::LABEL, id, text, Texture(), x, y, -1, -1, r, g, b);
}
void Menu::addButton(int id, const string& text, int x, int y, float r, float g, float b)
{
    items.emplace_back(MenuItem::BUTTON, id, text, Texture(), x, y, -1, -1, r, g, b);
}
void Menu::addImage(int id, Texture texture, int x, int y, int w, int h, float r, float g, float b)
{
    items.emplace_back(MenuItem::IMAGE, id, "", texture, x, y, w, h, r, g, b);
}
void Menu::addButtonImage(int id, Texture texture, int x, int y, int w, int h, float r, float g, float b)
{
    items.emplace_back(MenuItem::IMAGEBUTTON, id, "", texture, x, y, w, h, r, g, b);
}
void Menu::addMapLine(int x, int y, int w, int h, float startsize, float endsize, float r, float g, float b)
{
    items.emplace_back(MenuItem::MAPLINE, -1, "", Texture(), x, y, w, h, r, g, b, startsize, endsize);
}
void Menu::addMapMarker(int id, Texture texture, int x, int y, int w, int h, float r, float g, float b)
{
    items.emplace_back(MenuItem::MAPMARKER, id, "", texture, x, y, w, h, r, g, b);
}
void Menu::addMapLabel(int id, const string& text, int x, int y, float r, float g, float b)
{
    items.emplace_back(MenuItem::MAPLABEL, id, text, Texture(), x, y, -1, -1, r, g, b);
}

void Menu::setText(int id, const string& text)
{
    for (vector<MenuItem>::iterator it = items.begin(); it != items.end(); it++) {
        if (it->id == id) {
            it->text = text;
            it->w = it->text.length() * 10;
            break;
        }
    }
}

void Menu::setText(int id, const string& text, int x, int y, int w, int h)
{
    for (vector<MenuItem>::iterator it = items.begin(); it != items.end(); it++) {
        if (it->id == id) {
            it->text = text;
            it->x = x;
            it->y = y;
            if (w == -1) {
                it->w = it->text.length() * 10;
            }
            if (h == -1) {
                it->h = 20;
            }
            break;
        }
    }
}

int Menu::getSelected(int mousex, int mousey)
{
    for (vector<MenuItem>::reverse_iterator it = items.rbegin(); it != items.rend(); it++) {
        if (it->type == MenuItem::BUTTON || it->type == MenuItem::IMAGEBUTTON || it->type == MenuItem::MAPMARKER) {
            int mx = mousex;
            int my = mousey;
            if (it->type == MenuItem::MAPMARKER) {
                mx -= 1;
                my += 2;
            }
            if (mx >= it->x && mx < it->x + it->w && my >= it->y && my < it->y + it->h) {
                return it->id;
            }
        }
    }
    return -1;
}

void Menu::handleFadeEffect()
{
    for (vector<MenuItem>::iterator it = items.begin(); it != items.end(); it++) {
        if (it->id == Game::selected) {
            it->effectfade += multiplier * 5;
            if (it->effectfade > 1) {
                it->effectfade = 1;
            }
        } else {
            it->effectfade -= multiplier * 5;
            if (it->effectfade < 0) {
                it->effectfade = 0;
            }
        }
    }
}

void Menu::drawItems()
{
    handleFadeEffect();
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_ALPHA_TEST);
    glEnable(GL_BLEND);
    for (vector<MenuItem>::iterator it = items.begin(); it != items.end(); it++) {
        switch (it->type) {
            case MenuItem::IMAGE:
            case MenuItem::IMAGEBUTTON:
            case MenuItem::MAPMARKER:
                glColor4f(it->r, it->g, it->b, 1);
                glPushMatrix();
                if (it->type == MenuItem::MAPMARKER) {
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                    glTranslatef(2.5, -4.5, 0); // from old code
                } else {
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                }
                it->texture.bind();
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
                glBegin(GL_QUADS);
                glTexCoord2f(0, 0);
                glVertex3f(it->x, it->y, 0);
                glTexCoord2f(1, 0);
                glVertex3f(it->x + it->w, it->y, 0);
                glTexCoord2f(1, 1);
                glVertex3f(it->x + it->w, it->y + it->h, 0);
                glTexCoord2f(0, 1);
                glVertex3f(it->x, it->y + it->h, 0);
                glEnd();
                if (it->type != MenuItem::IMAGE) {
                    // mouseover highlight
                    for (int i = 0; i < 10; i++) {
                        if (1 - ((float)i) / 10 - (1 - it->effectfade) > 0) {
                            glColor4f(it->r, it->g, it->b, (1 - ((float)i) / 10 - (1 - it->effectfade)) * .25);
                            glBegin(GL_QUADS);
                            glTexCoord2f(0, 0);
                            glVertex3f(it->x - ((float)i) * 1 / 2, it->y - ((float)i) * 1 / 2, 0);
                            glTexCoord2f(1, 0);
                            glVertex3f(it->x + it->w + ((float)i) * 1 / 2, it->y - ((float)i) * 1 / 2, 0);
                            glTexCoord2f(1, 1);
                            glVertex3f(it->x + it->w + ((float)i) * 1 / 2, it->y + it->h + ((float)i) * 1 / 2, 0);
                            glTexCoord2f(0, 1);
                            glVertex3f(it->x - ((float)i) * 1 / 2, it->y + it->h + ((float)i) * 1 / 2, 0);
                            glEnd();
                        }
                    }
                }
                glPopMatrix();
                break;
            case MenuItem::LABEL:
            case MenuItem::BUTTON:
                glColor4f(it->r, it->g, it->b, 1);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                Game::text->glPrint(it->x, it->y, it->text.c_str(), 0, 1, 640, 480);
                if (it->type != MenuItem::LABEL) {
                    // mouseover highlight
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                    for (int i = 0; i < 15; i++) {
                        if (1 - ((float)i) / 15 - (1 - it->effectfade) > 0) {
                            glColor4f(it->r, it->g, it->b, (1 - ((float)i) / 10 - (1 - it->effectfade)) * .25);
                            Game::text->glPrint(it->x - ((float)i), it->y, it->text.c_str(), 0, 1 + ((float)i) / 70, 640, 480);
                        }
                    }
                }
                break;
            case MenuItem::MAPLABEL:
                Game::text->glPrintOutlined(0.9, 0, 0, 1, it->x, it->y, it->text.c_str(), 0, 0.6, 640, 480);
                break;
            case MenuItem::MAPLINE: {
                XYZ linestart;
                linestart.x = it->x;
                linestart.y = it->y;
                linestart.z = 0;
                XYZ lineend;
                lineend.x = it->x + it->w;
                lineend.y = it->y + it->h;
                lineend.z = 0;
                XYZ offset = lineend - linestart;
                XYZ fac = offset;
                Normalise(&fac);
                offset = DoRotation(offset, 0, 0, 90);
                Normalise(&offset);

                linestart += fac * 4 * it->linestartsize;
                lineend -= fac * 4 * it->lineendsize;

                glDisable(GL_TEXTURE_2D);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glColor4f(it->r, it->g, it->b, 1);
                glPushMatrix();
                glTranslatef(2, -5, 0); // from old code
                glBegin(GL_QUADS);
                glVertex3f(linestart.x - offset.x * it->linestartsize, linestart.y - offset.y * it->linestartsize, 0.0f);
                glVertex3f(linestart.x + offset.x * it->linestartsize, linestart.y + offset.y * it->linestartsize, 0.0f);
                glVertex3f(lineend.x + offset.x * it->lineendsize, lineend.y + offset.y * it->lineendsize, 0.0f);
                glVertex3f(lineend.x - offset.x * it->lineendsize, lineend.y - offset.y * it->lineendsize, 0.0f);
                glEnd();
                glPopMatrix();
                glEnable(GL_TEXTURE_2D);
            } break;
            default:
            case MenuItem::NONE:
                break;
        }
    }
}

void Menu::updateDifficultySelect(int selectedDifficulty)
{
    flash();
    clearMenu();

    int descriptionY = 400;
    int descriptionX = 200;
    // Add the difficulty options
    addButton(0, "Easier", 10, 400);
    addButton(1, "Difficult", 10, 360);
    addButton(2, "Insane", 10, 320);

    // Array to store the description lines
    std::vector<std::string> descriptionLines;

    // Populate the array based on the selected difficulty
    switch (selectedDifficulty) {
        case 0: // Easier
            descriptionLines.push_back("Easier:");
            descriptionLines.push_back("Player has extra resilience");
            descriptionLines.push_back("Enemies have less resilience");
            newdifficulty = 0;
            break;
        case 1: // Difficult
            descriptionLines.push_back("Difficult:");
            descriptionLines.push_back("Players has normal resilience");
            descriptionLines.push_back("Enemies have normal resilience.");
            newdifficulty = 1;
            break;
        case 2: // Insane
            descriptionLines.push_back("Insane:");
            descriptionLines.push_back("Player has less resilience");
            descriptionLines.push_back("Enemies have extra resilience");
            descriptionLines.push_back("No Minimap");
            newdifficulty = 2;
            break;
        default:
            descriptionLines.push_back("Select a difficulty level:");
            descriptionX = 200;
            descriptionY = 420;
            break;
    }

    for (const std::string& line : descriptionLines) {
        addLabel(-1, line, descriptionX, descriptionY);
        descriptionY -= 20;
    }

    addButton(3, "Choose Selected", 10, 280);
    addButton(4, "Back", 10, 10);
}

void Menu::updateSettingsMenu(int submenu)
{
    flash();
    clearMenu();
    string activeMenu = "";
    string detailText = "";
    string bloodText = "";
    int itemX = 240;
    int itemY = 370;  // Starting Y position for submenu items
    int yOffset = 35; // Y offset for each subsequent item

    // Always present buttons
    addButton(1, "Video", 10, 380);
    addButton(2, "Audio", 10, 320);
    addButton(3, "Gameplay", 10, 260);
    addButton(4, "Controls", 10, 200);

    std::string newResolution = std::string("Resolution: ") + to_string(newscreenwidth) + "*" + to_string(newscreenheight);
    if (((float)newscreenwidth <= (float)newscreenheight * 1.61) && ((float)newscreenwidth >= (float)newscreenheight * 1.59)) {
        newResolution += " (widescreen)";
    }
    if (newdetail == 0) {
        detailText = "Detail: Low";
    } else if (newdetail == 1) {
        detailText = "Detail: Medium";
    } else if (newdetail == 2) {
        detailText = "Detail: High";
    }

    if (bloodtoggle == 0) {
        bloodText = "Blood: Off";
    } else if (bloodtoggle == 1) {
        bloodText = "Blood: Low";
    } else if (bloodtoggle == 2) {
        bloodText = "Blood: High";
    }

    std::vector<std::pair<int, std::string>> items; // List of submenu items with their IDs and text

    // Submenu specific items
    switch (submenu) {
        case 1: // Video settings
            items = {
                { 101, newResolution },
                { 102, detailText },
                { 103, decalstoggle ? "Decals: Enabled" : "Decals: Disabled" },
                { 104, fullscreen ? "Fullscreen: On" : "Fullscreen: Off" },
                { 105, std::string("3D Stereo mode: ") + StereoModeName(newstereomode) }
            };
            if (newstereomode != stereoNone) {
                items.push_back({ 106, std::string("Stereo separation: ") + to_string(stereoseparation) });
                items.push_back({ 107, std::string("Reverse stereo: ") + (stereoreverse ? "Yes" : "No") });
            }
            activeMenu = "Video Settings...";
            break;
        case 2: // Audio settings
            items = {
                { 201, musictoggle ? "Music: Enabled" : "Music: Disabled" },
                { 202, std::string("Music Volume: ") + to_string(int(AudioManager::musicVolume * 100)) + "%" },
                { 203, std::string("Sound Effects Volume: ") + to_string(int(AudioManager::soundEffectsVolume * 100)) + "%" }
            };
            activeMenu = "Audio Settings...";
            break;
        case 3: // Gameplay settings
            items = {
                { 301, bloodText },
                { 302, ismotionblur ? "Blur Effects: Enabled" : "Blur Effects: Disabled" },
                { 303, showdamagebar ? "Damage Bar: On" : "Damage Bar: Off" },
                { 304, showenemydamagebars ? "Enemy Damage Bars: On" : "Enemy Damage Bars: Off" }
            };
            activeMenu = "Gameplay Settings...";
            break;
        case 4: // Controls settings
            items = {
                { 401, invertmouse ? "Invert mouse: Yes" : "Invert mouse: No" },
                { 402, std::string("Mouse Speed: ") + to_string(int(usermousesensitivity * 5)) },
                { 403, (string) "Forwards: " + (keyselect == 403 ? "_" : Input::keyToChar(forwardkey)) },
                { 404, (std::string) "Back: " + (keyselect == 404 ? "_" : Input::keyToChar(backkey)) },
                { 405, (std::string) "Left: " + (keyselect == 405 ? "_" : Input::keyToChar(leftkey)) },
                { 406, (std::string) "Right: " + (keyselect == 406 ? "_" : Input::keyToChar(rightkey)) },
                { 407, (std::string) "Crouch: " + (keyselect == 407 ? "_" : Input::keyToChar(crouchkey)) },
                { 408, (std::string) "Jump: " + (keyselect == 408 ? "_" : Input::keyToChar(jumpkey)) },
                { 409, (std::string) "Draw: " + (keyselect == 409 ? "_" : Input::keyToChar(drawkey)) },
                { 410, (std::string) "Throw: " + (keyselect == 410 ? "_" : Input::keyToChar(throwkey)) },
                { 411, (std::string) "Attack: " + (keyselect == 411 ? "_" : Input::keyToChar(attackkey)) }
            };
            if (devtools) {
                items.push_back({ 412, (std::string) "Console: " + (keyselect == 412 ? "_" : Input::keyToChar(consolekey)) });
            }
            activeMenu = "Configure controls...";
            break;
    }

    // Loop to place submenu items dynamically
    for (const auto& item : items) {
        addButton(item.first, item.second, itemX, itemY);
        itemY -= yOffset; // Decrement Y position for the next item
    }

    // Add back button
    addButton(0, "Back", 10, 10);

    if ((newdetail == detail) && (newscreenheight == (int)screenheight) && (newscreenwidth == (int)screenwidth)) {
        setText(0, "Back");
    } else {
        setText(0, "Back (some changes take effect next time Lugaru is opened)");
    }

    addLabel(5, activeMenu, 240, 420);
}

/*
 *Values of mainmenu :
 *1 Main menu
 *2 Menu pause (resume/end game)
 *3 Option menu
 *5 Main game menu (choose level or challenge)
 *6 Deleting user menu
 *7 User managment menu (select/add)
 *8 Choose difficulty menu
 *9 Challenge level selection menu
 *10 End of the campaign congratulation (is that really a menu?)
 *11 Same that 9 ??? => unused
 */
void Menu::Load()
{
    clearMenu();
    switch (mainmenu) {
        case 1:
        case 2:
            addImage(0, Mainmenuitems[0], 150, 480 - 128, 256, 128);
            addButtonImage(1, Mainmenuitems[mainmenu == 1 ? 1 : 5], 18, mainmenu == 1 ? 296 : 320, 128, 32);
            addButtonImage(2, Mainmenuitems[2], 18, mainmenu == 1 ? 220 : 160, 112, 32);
            if (mainmenu == 2) {
                addButtonImage(3, Mainmenuitems[8], 18, 240, 128, 32);
            }
            addButtonImage(4, Mainmenuitems[mainmenu == 1 ? 3 : 6], 18, mainmenu == 1 ? 142 : 80, mainmenu == 1 ? 68 : 132, 32); // Quit if main menu, EndGame if pause menu
            addLabel(-1, VERSION_NUMBER + VERSION_SUFFIX, 640 - 100, 10);
            break;
        case 3:
            updateSettingsMenu(1);
            break;
        case 5: {
            LoadCampaign();
            addLabel(-1, Account::active().getName(), 5, 400);
            addButton(1, "Tutorial", 5, 300);
            addButton(2, "Challenge", 5, 240);
            addButton(3, "Edit User", 400, 10);
            addButton(4, "Main Menu", 5, 10);
            addButton(5, "Change User", 5, 180);
            addButton(6, "Campaign : " + Account::active().getCurrentCampaign(), 200, 420);

            // show campaign map
            // with (2,-5) offset from old code
            addImage(-1, Mainmenuitems[7], 150 + 2, 60 - 5, 400, 400);
            // show levels
            int numlevels = Account::active().getCampaignChoicesMade();
            numlevels += numlevels > 0 ? campaignlevels[numlevels - 1].nextlevel.size() : 1;
            for (int i = 0; i < numlevels; i++) {
                XYZ midpoint = campaignlevels[i].getCenter();
                float itemsize = campaignlevels[i].getWidth();
                const bool active = (i >= Account::active().getCampaignChoicesMade());
                if (!active) {
                    itemsize /= 2;
                }

                if (i >= 1) {
                    XYZ start = campaignlevels[i - 1].getCenter();
                    addMapLine(start.x, start.y, midpoint.x - start.x, midpoint.y - start.y, 0.5, active ? 1 : 0.5, active ? 1 : 0.5, 0, 0);
                }
                addMapMarker(NB_CAMPAIGN_MENU_ITEM + i, Mapcircletexture, midpoint.x - itemsize / 2, midpoint.y - itemsize / 2, itemsize, itemsize, active ? 1 : 0.5, 0, 0);

                if (active) {
                    addMapLabel(-2, campaignlevels[i].description, campaignlevels[i].getStartX() + 10, campaignlevels[i].getStartY() - 4);
                }
            }
        } break;
        case 6:
            addLabel(-1, "Are you sure you want to delete this user?", 10, 400);
            addButton(1, "Yes", 10, 360);
            addButton(2, "No", 10, 320);
            break;
        case 7:
            if (Account::getNbAccounts() < 8) {
                addButton(0, "New User", 10, 400);
            } else {
                addLabel(0, "No More Users", 10, 400);
            }
            addLabel(-2, "", 20, 400);
            addButton(Account::getNbAccounts() + 1, "Back", 10, 10);
            for (int i = 0; i < Account::getNbAccounts(); i++) {
                addButton(i + 1, Account::get(i).getName(), 10, 340 - 20 * (i + 1));
            }
            break;
        case 8:                         // Difficulty selection menu
            updateDifficultySelect(-1); // Pass -1 to show the default message initially
            break;
        case 9:
            for (int i = 0; i < numchallengelevels; i++) {
                string name = "Level ";
                name += to_string(i + 1);
                if (name.size() < 17) {
                    name.append((17 - name.size()), ' ');
                }

                // High Score
                name += to_string(int(Account::active().getHighScore(i)));
                if (name.size() < 32) {
                    name.append((32 - name.size()), ' ');
                }

                // Best Time
                int fasttime = (int)round(Account::active().getFastTime(i));
                name += to_string(int((fasttime - fasttime % 60) / 60)); // Minutes
                name += ":";
                if (fasttime % 60 < 10) {
                    name += "0"; // Add leading zero if seconds < 10
                }
                name += to_string(fasttime % 60); // Seconds

                // Ensure consistent spacing after time, if necessary
                if (name.size() < 48) { // Adjust the size based on your layout
                    name.append((48 - name.size()), ' ');
                }

                // Flawless
                bool flawless = Account::active().getFlawless(i);
                name += flawless ? "Yes" : "No";

                // Add the button with the constructed name
                addButton(i, name, 10, 400 - i * 25, i > Account::active().getProgress() ? 0.5 : 1, 0, 0);
            }

            // Add the title header for the columns
            addLabel(-1, "             High Score      Best Time       Flawless?", 10, 440);
            addButton(numchallengelevels, "Back", 10, 10);
            break;

        case 10: {
            addLabel(0, campaignEndText[0], 80, 330);
            addLabel(1, campaignEndText[1], 80, 300);
            addLabel(2, campaignEndText[2], 80, 270);
            addButton(3, "Back", 10, 10);
            addLabel(4, string("Your score:         ") + to_string((int)Account::active().getCampaignScore()), 190, 200);
            addLabel(5, string("Highest score:      ") + to_string((int)Account::active().getCampaignHighScore()), 190, 180);
        } break;
        case 12: // New Edit User menu case
            addLabel(-1, "Edit User Profile - " + Account::active().getName(), 200, 420);
            addButton(0, "", 10, 400);
            addLabel(-2, "", 10, 400);
            addButton(1, "Change Name", 10, 360);
            addButton(2, "Change Difficulty", 10, 320);
            addButton(3, "Delete User", 10, 280);
            addButton(4, "Back", 10, 10);
            break;
    }
}

void Menu::startChallengeLevel(int selected)
{
    fireSound();
    flash();

    startbonustotal = 0;

    loading = 2;
    loadtime = 0;
    targetlevel = selected;
    if (firstLoadDone) {
        TickOnceAfter();
    } else {
        LoadStuff();
    }
    LoadLevel(selected);
    campaign = 0;

    mainmenu = 0;
    gameon = 1;
    AudioManager::stopTrack();
}

void Menu::Tick()
{
    // escape key pressed
    if (Input::isKeyPressed(SDL_SCANCODE_ESCAPE) &&
        (mainmenu >= 3) && (mainmenu != 8) && !((mainmenu == 7) && entername) && !((mainmenu == 12) && changeusername)) {
        selected = -1;

        // finished with settings menu
        if (mainmenu == 3) {
            SaveSettings();
        }
        // effects
        if (mainmenu >= 3 && mainmenu != 8) {
            fireSound();
            flash();
        }
        // go back
        switch (mainmenu) {
            case 3:
            case 5:
                mainmenu = gameon ? 2 : 1;
                break;
            case 6:
            case 7:
            case 9:
            case 10:
            case 12:
                mainmenu = 5;
                break;
        }
    }

    // menu buttons
    selected = getSelected(mousecoordh * 640 / screenwidth, 480 - mousecoordv * 480 / screenheight);

    // some specific case where we do something even if the left mouse button is not pressed.
    if ((mainmenu == 5) && (endgame == 2)) {
        Account::active().endGame();
        endgame = 0;
    }

    if (mainmenu == 10) {
        endgame = 2;
    }

    // Adjust backwards
    if (mainmenu == 3 && Input::isKeyPressed(MOUSEBUTTON_RIGHT) && selected == 106) {
        stereoseparation -= 0.001;
        updateSettingsMenu(1);
    }

    static int oldmainmenu = mainmenu;

    if (Input::MouseClicked() && (selected >= 0)) { // handling of the left mouse click in menus
        set<pair<int, int>>::iterator newscreenresolution;

        switch (mainmenu) {
            case 1:
            case 2:
                switch (selected) {
                    case 1:           // Resume or New Game
                        if (gameon) { // resume
                            mainmenu = 0;
                            AudioManager::changeTrack(leveltheme);
                        } else { // new game
                            fireSound(SoundID::FireStart);
                            flash();
                            mainmenu = (Account::hasActive() ? 5 : 7);
                            selected = -1;
                        }
                        break;
                    case 2: // options
                        fireSound();
                        flash();
                        mainmenu = 3;
                        if (newdetail > 2) {
                            newdetail = detail;
                        }
                        if (newdetail < 0) {
                            newdetail = detail;
                        }
                        if (newscreenwidth > 3000) {
                            newscreenwidth = screenwidth;
                        }
                        if (newscreenwidth < 0) {
                            newscreenwidth = screenwidth;
                        }
                        if (newscreenheight > 3000) {
                            newscreenheight = screenheight;
                        }
                        if (newscreenheight < 0) {
                            newscreenheight = screenheight;
                        }
                        break;
                    case 3: // Restart
                        fireSound();
                        flash();
                        if (gameon) {
                            mainmenu = 0;
                            fireSound(SoundID::FireStart);
                            Game::RestartLevel();
                            AudioManager::changeTrack(leveltheme);
                        }
                        break;
                    case 4: // End Game or Quit
                        fireSound();
                        flash();
                        if (gameon) {
                            gameon = 0;
                            mainmenu = 1;
                        } else {
                            tryquit = 1;
                            AudioManager::stopTrack();
                        }
                        break;
                }
                break;

            case 3: // Options menu
                fireSound();
                switch (selected) {
                    case 0: // Save and Go Back
                        SaveSettings();
                        mainmenu = gameon ? 2 : 1;
                        break;
                    case 1: // Video settings
                        updateSettingsMenu(1);
                        break;
                    case 2: // Audio settings
                        updateSettingsMenu(2);
                        break;
                    case 3: // Gameplay settings
                        updateSettingsMenu(3);
                        break;
                    case 4: // Controls settings
                        updateSettingsMenu(4);
                        break;

                    // Video settings cases
                    case 101: {
                        if (resolutions.empty()) {
                            fprintf(stderr, "Error: resolutions is empty in Menu::Tick().\n");
                        }
                        
                        auto it = resolutions.find(std::make_pair(newscreenwidth, newscreenheight));
                        // If not found, start from the beginning;
                        // otherwise, move to the next resolution.
                        if (it == resolutions.end()) {
                            it = resolutions.begin();
                        } else {
                            ++it;
                            if (it == resolutions.end()) {
                                it = resolutions.begin();
                            }
                        }

                        if (!resolutions.empty()) {
                            auto it = resolutions.find(std::make_pair(newscreenwidth, newscreenheight));
                            if (it == resolutions.end()) {
                                it = resolutions.begin(); // Wrap around
                            } else {
                                ++it;
                                if (it == resolutions.end()) {
                                    it = resolutions.begin(); // Wrap around again
                                }
                            }

                            newscreenwidth = it->first;
                            newscreenheight = it->second;
                        }

                        updateSettingsMenu(1);
                    } break;
                    case 102: // Change detail level
                        newdetail = (newdetail + 1) % 3;
                        updateSettingsMenu(1);
                        break;
                    case 103:
                        decalstoggle = !decalstoggle;
                        updateSettingsMenu(1);
                        break;
                    case 104: // Toggle fullscreen
                        toggleFullscreen();
                        updateSettingsMenu(1);
                        break;
                    case 105: // Change stereo mode
                        newstereomode = (StereoMode)((newstereomode + 1) % stereoCount);
                        InitStereo(newstereomode);
                        updateSettingsMenu(1);
                        break;
                    case 106: // Adjust stereo separation
                        stereoseparation += 0.001f;
                        updateSettingsMenu(1);
                        break;
                    case 107: // Toggle reverse stereo
                        stereoreverse = !stereoreverse;
                        updateSettingsMenu(1);
                        break;

                    // Audio settings cases
                    case 201: // Toggle music
                        musictoggle = !musictoggle;
                        if (musictoggle) {
                            AudioManager::changeTrack(TrackID::MenuTheme);
                        } else {
                            // Pausing specific music streams
                            AudioManager::stopTrack();
                            std::fill(std::begin(musicvolume), std::end(musicvolume), 0);
                        }
                        updateSettingsMenu(2);
                        break;
                    case 202: // Adjust music volume
                        AudioManager::setMusicVolume(AudioManager::musicVolume + 0.1f > 1.0f ? 0.0f : AudioManager::musicVolume + 0.1f);
                        updateSettingsMenu(2);
                        break;

                    case 203: // Adjust sound effects volume
                        AudioManager::setSoundEffectsVolume(AudioManager::soundEffectsVolume + 0.1f > 1.0f ? 0.0f : AudioManager::soundEffectsVolume + 0.1f);
                        updateSettingsMenu(2);
                        break;

                    // Gameplay settings cases
                    case 301: // Toggle blood
                        bloodtoggle = (bloodtoggle + 1) % 3;
                        updateSettingsMenu(3);
                        break;
                    case 302: // Toggle blur
                        ismotionblur = !ismotionblur;
                        updateSettingsMenu(3);
                        break;
                    case 303: // Toggle damage bar
                        showdamagebar = !showdamagebar;
                        updateSettingsMenu(3);
                        break;
                    case 304: // Toggle enemy damage bars
                        showenemydamagebars = !showenemydamagebars;
                        updateSettingsMenu(3);
                        break;

                    // Controls settings cases
                    case 401: // Toggle invert mouse
                        invertmouse = !invertmouse;
                        updateSettingsMenu(4);
                        break;
                    case 402: // Adjust mouse speed
                        usermousesensitivity = (usermousesensitivity + 0.2f > 2.0f) ? 0.2f : usermousesensitivity + 0.2f;
                        updateSettingsMenu(4);
                        break;
                    /* Keybinds */
                    case 403:
                    case 404:
                    case 405:
                    case 406:
                    case 407:
                    case 408:
                    case 409:
                    case 410:
                    case 411:
                    case 412:
                        // Check if we're waiting for key selection
                        if (!waiting) {
                            fireSound();
                            if (selected < (devtools ? 412 : 411) && keyselect == -1) {
                                keyselect = selected - 403; // Adjust for the controls menu offset
                            }
                            if (keyselect != -1) {
                                setKeySelected();
                            }
                        }
                        updateSettingsMenu(4);
                        break;
                }
                break;
                break;
            case 5:
                fireSound();
                flash();
                if ((selected - NB_CAMPAIGN_MENU_ITEM >= Account::active().getCampaignChoicesMade())) {
                    startbonustotal = 0;

                    loading = 2;
                    loadtime = 0;
                    targetlevel = 7;
                    if (firstLoadDone) {
                        TickOnceAfter();
                    } else {
                        LoadStuff();
                    }
                    whichchoice = selected - NB_CAMPAIGN_MENU_ITEM - Account::active().getCampaignChoicesMade();
                    actuallevel = (Account::active().getCampaignChoicesMade() > 0 ? campaignlevels[Account::active().getCampaignChoicesMade() - 1].nextlevel[whichchoice] : 0);
                    visibleloading = true;
                    stillloading = 1;
                    LoadLevel(campaignlevels[actuallevel].mapname.c_str());
                    campaign = 1;
                    mainmenu = 0;
                    gameon = 1;
                    AudioManager::stopTrack();
                }
                switch (selected) {
                    case 1:
                        startbonustotal = 0;

                        loading = 2;
                        loadtime = 0;
                        targetlevel = -1;
                        if (firstLoadDone) {
                            TickOnceAfter();
                        } else {
                            LoadStuff();
                        }
                        LoadLevel(-1);

                        mainmenu = 0;
                        gameon = 1;
                        AudioManager::stopTrack();
                        break;
                    case 2:
                        mainmenu = 9;
                        break;
                    case 3:
                        mainmenu = 12;
                        break;
                    case 4:
                        mainmenu = (gameon ? 2 : 1);
                        break;
                    case 5:
                        mainmenu = 7;
                        break;
                    case 6:
                        vector<string> campaigns = ListCampaigns();
                        vector<string>::iterator c;
                        if ((c = find(campaigns.begin(), campaigns.end(), Account::active().getCurrentCampaign())) == campaigns.end()) {
                            if (!campaigns.empty()) {
                                Account::active().setCurrentCampaign(campaigns.front());
                            }
                        } else {
                            c++;
                            if (c == campaigns.end()) {
                                c = campaigns.begin();
                            }
                            Account::active().setCurrentCampaign(*c);
                        }
                        Load();
                        break;
                }
                break;
            case 6:
                fireSound();
                if (selected == 1) {
                    flash();
                    Account::destroyActive();
                    mainmenu = 7;
                } else if (selected == 2) {
                    flash();
                    mainmenu = 5;
                }
                break;

            case 7:
                fireSound();
                if (selected == 0 && Account::getNbAccounts() < 8) {
                    entername = 1;
                } else if (selected < Account::getNbAccounts() + 1) {
                    flash();
                    mainmenu = 5;
                    Account::setActive(selected - 1);
                } else if (selected == Account::getNbAccounts() + 1) {
                    flash();
                    if (Account::hasActive()) {
                        mainmenu = 5;
                    } else {
                        mainmenu = 1;
                    }
                    newusername.clear();
                    newuserselected = 0;
                    entername = 0;
                }
                break;

            case 8:
                fireSound();
                flash();

                // Check which difficulty level is clicked
                if (selected <= 2) {
                    updateDifficultySelect(selected);               // Update the description on the right
                } else if (selected == 3) {                         // Choose Selected
                    Account::active().setDifficulty(newdifficulty); // Apply the selected difficulty
                    mainmenu = 5;                                   // Go back to the campaign menu
                } else if (selected == 4) {                         // Back button
                    mainmenu = 5;                                   // Return to the campaign menu
                }
                break;
            case 9:
                if (selected < numchallengelevels && selected <= Account::active().getProgress()) {
                    startChallengeLevel(selected);
                }
                if (selected == numchallengelevels) {
                    fireSound();
                    flash();
                    mainmenu = 5;
                }
                break;

            case 10:
                if (selected == 3) {
                    fireSound();
                    flash();
                    mainmenu = 5;
                }
                break;
            case 12: // Edit User Profile menu
                switch (selected) {
                    case 1:
                        changeusername = 1;
                        break;
                    case 2:
                        fireSound();
                        flash();
                        mainmenu = 8;
                        break;
                    case 3:
                        fireSound();
                        flash();
                        mainmenu = 6; // Delete user
                        break;
                    case 4:
                        AudioManager::stopAllSounds();
                        fireSound();
                        flash();
                        mainmenu = 5; // Back to the campaign menu
                        break;
                }
                break;
        }
    }

    AudioManager::setTrackPitch(TrackID::MenuTheme);

    if (entername) {
        inputText(newusername, &newuserselected);
        if (!waiting) {                 // the input as finished
            if (!newusername.empty()) { // with enter
                Account::add(string(newusername));

                mainmenu = 8;

                flash();

                fireSound(SoundID::FireStart);

                newusername.clear();

                newuserselected = 0;
            }
            entername = 0;
            Load();
        }

        newuserblinkdelay -= multiplier;
        if (newuserblinkdelay <= 0) {
            newuserblinkdelay = .3;
            newuserblink = !newuserblink;
        }
    }

    if (changeusername) {
        inputText(newusername, &newuserselected);
        if (!waiting) {
            if (!newusername.empty()) {
                Account::renameAccount(Account::active(), newusername);
                changeusername = 0;
                flash();
                fireSound(SoundID::FireStart);
                newusername.clear();
                newuserselected = 0;
                Load();
            }
        }

        newuserblinkdelay -= multiplier;
        if (newuserblinkdelay <= 0) {
            newuserblinkdelay = .3;
            newuserblink = !newuserblink;
        }
    }

    if (entername || changeusername) {
        setText(0, newusername, 20, 400, -1, -1);
        setText(-2, newuserblink ? "_" : "", 20 + newuserselected * 10, 400, -1, -1);
    }

    if (changeusername && Input::isKeyPressed(SDL_SCANCODE_ESCAPE)) {
        changeusername = 0;
        newusername.clear();
        newuserselected = 0;
        fireSound();
        flash();
        Load();
    }

    if (oldmainmenu != mainmenu) {
        Load();
    }
    oldmainmenu = mainmenu;
}

int setKeySelected_thread(void*)
{
    using namespace Game;
    int scancode = -1;
    SDL_Event evenement;

    waiting = true;

    // Loop to wait for a key or mouse button press
    while (scancode == -1) {
        SDL_WaitEvent(&evenement);

        switch (evenement.type) {
            case SDL_KEYDOWN:
                scancode = evenement.key.keysym.scancode;
                break;
            case SDL_MOUSEBUTTONDOWN:
                // Only bind left or right click to keys when appropriate
                if (evenement.button.button == SDL_BUTTON_LEFT || evenement.button.button == SDL_BUTTON_RIGHT) {
                    scancode = SDL_NUM_SCANCODES + evenement.button.button;
                }
                break;
            default:
                break;
        }
    }

    // If the user presses ESC, we cancel the key selection process
    if (scancode != SDL_SCANCODE_ESCAPE) {
        fireSound();
        switch (keyselect) {
            case 0:
                forwardkey = scancode;
                break;
            case 1:
                backkey = scancode;
                break;
            case 2:
                leftkey = scancode;
                break;
            case 3:
                rightkey = scancode;
                break;
            case 4:
                crouchkey = scancode;
                break;
            case 5:
                jumpkey = scancode;
                break;
            case 6:
                drawkey = scancode;
                break;
            case 7:
                throwkey = scancode;
                break;
            case 8:
                attackkey = scancode;
                break;
            case 9:
                consolekey = scancode;
                break;
            default:
                break;
        }
        // Add a slight delay to prevent the mouse from being "clamped"
        SDL_Delay(300); // Delay for 300 milliseconds (adjust as necessary)
    }

    keyselect = -1;
    waiting = false;

    // Return to the controls menu
    Menu::updateSettingsMenu(4);

    return 0;
}

void Menu::setKeySelected()
{
    waiting = true;
    printf("launch thread\n");
    SDL_Thread* thread = SDL_CreateThread(setKeySelected_thread, NULL, NULL);
    if (thread == NULL) {
        fprintf(stderr, "Unable to create thread: %s\n", SDL_GetError());
        waiting = false;
        return;
    }
}
