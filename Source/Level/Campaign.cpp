/*
Copyright (C) 2003, 2010 - Wolfire Games
Copyright (C) 2010-2017 - Lugaru contributors (see AUTHORS file)

This file is part of Lugaru.

Lugaru is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Lugaru is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lugaru.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Level/Campaign.hpp"

#include "Game.hpp"
#include "Utils/Folders.hpp"
#include <json/json.h>

#include <dirent.h>

using namespace Game;

std::vector<CampaignLevel> campaignlevels;

bool campaign = false;

int actuallevel = 0;
std::string campaignEndText[3];

std::vector<std::string> ListCampaigns()
{
    std::vector<std::string> campaignNames;

    // Check for base game campaigns (Lugaru)
    std::string lugaruCampaignPath = Folders::getResourcePath("Lugaru/Campaign.json");
    if (Folders::file_exists(lugaruCampaignPath)) {
        campaignNames.push_back("Lugaru");
        std::cerr << "Found base game campaign: Lugaru" << std::endl;
    }

    // Now check for campaigns in mod packs by searching for PackName/Campaign.json
    std::string packListPath = Folders::getResourcePath("PackList.json");
    if (!Folders::file_exists(packListPath)) {
        std::cerr << "PackList.json not found!" << std::endl;
        return campaignNames;
    }

    // Open and read PackList.json to get enabled mods
    std::ifstream packListFile(packListPath, std::ifstream::binary);
    Json::Value packListJson;
    Json::CharReaderBuilder readerBuilder;
    std::string errors;

    if (!Json::parseFromStream(readerBuilder, packListFile, &packListJson, &errors)) {
        std::cerr << "Error parsing PackList.json: " << errors << std::endl;
        return campaignNames;
    }

    const Json::Value& mods = packListJson["Packs"]["Mods"];

    // Iterate through each enabled mod and check if a Campaign.json file exists
    for (const auto& mod : mods) {
        if (mod["Status"].asString() == "Enabled") {
            std::string modName = mod["ModName"].asString();
            std::string modCampaignPath = Folders::getResourcePath(modName + "/Campaign.json");

            if (Folders::file_exists(modCampaignPath)) {
                // Found a campaign in this mod, add its name
                campaignNames.push_back(modName);
                std::cerr << "Found campaign in mod: " << modName << std::endl;
            }
        }
    }

    return campaignNames;
}

void LoadCampaign()
{
    if (!Account::hasActive()) {
        std::cerr << "No active account found!" << std::endl;
        return;
    }

    std::string campaignPath = Folders::getResourcePath(Account::active().getCurrentCampaign() + "/Campaign.json");

    // Try loading the campaign from the current pack
    std::ifstream campaignFile(campaignPath, std::ifstream::binary);
    if (!campaignFile.good()) {
        std::cerr << "Could not find campaign \"" << Account::active().getCurrentCampaign() << "\", falling back to base game." << std::endl;
        Account::active().setCurrentCampaign("Lugaru");
        LoadCampaign(); // Recursive call to load the base campaign
        return;
    }

    // Parse campaign data if the file is found
    Json::Value campaignData;
    std::string errors;
    Json::CharReaderBuilder readerBuilder;
    if (!Json::parseFromStream(readerBuilder, campaignFile, &campaignData, &errors)) {
        std::cerr << "Error parsing campaign JSON: " << errors << std::endl;
        return;
    }

    if (!campaignData.isMember("Levels")) {
        std::cerr << "Invalid campaign JSON structure!" << std::endl;
        return;
    }

    campaignlevels.clear();

    // Loop through each level in the campaign
    for (const auto& levelData : campaignData["Levels"]) {
        CampaignLevel cl;

        if (!levelData.isMember("Name") || !levelData.isMember("Description")) {
            std::cerr << "Level data missing mandatory fields!" << std::endl;
            continue;
        }

        cl.mapname = levelData["Name"].asString();
        cl.description = levelData["Description"].asString();
        std::replace(cl.description.begin(), cl.description.end(), '_', ' ');

        // Handle optional fields
        cl.choosenext = levelData.get("ChooseNext", 0).asInt(); // Default to 0 if not present
        cl.location.x = levelData.get("LocationX", 0).asInt();  // Default to (0,0) if not present
        cl.location.y = levelData.get("LocationY", 0).asInt();

        // Ensure next levels are read properly
        int numNext = levelData.get("NumNext", 0).asInt();
        if (numNext > 0 && levelData.isMember("NextLevel")) {
            if (levelData["NextLevel"].isArray()) {
                for (int i = 0; i < numNext; i++) {
                    cl.nextlevel.push_back(levelData["NextLevel"][i].asInt() - 1); // Convert to int and subtract 1
                }
            } else {
                cl.nextlevel.push_back(levelData["NextLevel"].asInt() - 1); // Convert to int and subtract 1
            }
        }

        campaignlevels.push_back(cl);
    }

    campaignEndText[0] = "Congratulations!";
    campaignEndText[1] = string("You have completed ") + Account::active().getCurrentCampaign() + " campaign";
    campaignEndText[2] = "and restored peace to the island of Lugaru.";

    // Load World.png texture with fallback logic
    std::string worldTexturePath = Folders::getResourcePath(Account::active().getCurrentCampaign() + "/Textures/World.png");

    if (worldTexturePath.empty()) {
        std::cerr << "World.png not found in the current campaign, trying fallback." << std::endl;
        worldTexturePath = Folders::getResourcePath("Lugaru/Textures/World.png");

        if (worldTexturePath.empty()) {
            std::cerr << "Fallback to base game World.png failed!" << std::endl;
        } else {
            std::cerr << "Loaded fallback World.png from base game." << std::endl;
        }
    }

    if (!worldTexturePath.empty()) {
        Mainmenuitems[7].load(worldTexturePath, 0);
    }

    // Reset campaign progress if no choices were made yet
    if (Account::active().getCampaignChoicesMade() == 0) {
        Account::active().setCampaignScore(0);
        Account::active().resetFasttime();
    }
}

CampaignLevel::CampaignLevel()
    : width(10)
    , choosenext(1)
{
    location.x = 0;
    location.y = 0;
}

int CampaignLevel::getStartX()
{
    return 30 + 120 + location.x * 400 / 512;
}

int CampaignLevel::getStartY()
{
    return 30 + 30 + (512 - location.y) * 400 / 512;
}

int CampaignLevel::getEndX()
{
    return getStartX() + width;
}

int CampaignLevel::getEndY()
{
    return getStartY() + width;
}

XYZ CampaignLevel::getCenter()
{
    XYZ center;
    center.x = getStartX() + width / 2;
    center.y = getStartY() + width / 2;
    return center;
}

int CampaignLevel::getWidth()
{
    return width;
}

istream& CampaignLevel::operator<<(istream& is)
{
    is.ignore(256, ':');
    is.ignore(256, ':');
    is.ignore(256, ' ');
    is >> mapname;
    is.ignore(256, ':');
    is >> description;
    for (size_t pos = description.find('_'); pos != string::npos; pos = description.find('_', pos)) {
        description.replace(pos, 1, 1, ' ');
    }
    is.ignore(256, ':');
    is >> choosenext;
    is.ignore(256, ':');
    int numnext, next;
    is >> numnext;
    for (int j = 0; j < numnext; j++) {
        is.ignore(256, ':');
        is >> next;
        nextlevel.push_back(next - 1);
    }
    is.ignore(256, ':');
    is >> location.x;
    is.ignore(256, ':');
    is >> location.y;
    return is;
}
