/*
Copyright (C) 2003, 2010 - Wolfire Games
Copyright (C) 2010-2017 - Lugaru contributors (see AUTHORS file)

This file is part of Lugaru.

Lugaru is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Lugaru is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lugaru.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Folders.hpp"

#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <json/json.h>
#include <unordered_set>
#include <algorithm>
#include <sys/stat.h>

#if PLATFORM_UNIX
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#endif

#ifdef _WIN32 
#include <shlobj.h> // to get paths related functions
#include <windows.h>
#include <direct.h>
#include <io.h>
#include <winbase.h>
#include <windef.h>
#define _stat stat
#define S_IFDIR _S_IFDIR
#endif




const std::string Folders::dataDir = DATA_DIR;

std::string Folders::getScreenshotDir()
{
    std::string screenshotDir = getUserDataPath() + "/Screenshots";
    makeDirectory(screenshotDir);
    return screenshotDir;
}

std::string Folders::getUserDataPath()
{
    std::string userDataPath;
#ifdef _WIN32
    char path[MAX_PATH];
    if (GetModuleFileNameA(nullptr, path, MAX_PATH)) {
        std::string exePath = path;
        std::string exeDir = exePath.substr(0, exePath.find_last_of("\\/"));
        userDataPath = exeDir + "/Data";
    } else {
        std::cerr << "Error retrieving executable path." << std::endl;
        return dataDir;
    }
#elif (defined(__APPLE__) && defined(__MACH__))
    const char* homePath = getHomeDirectory();
    if (homePath == NULL) {
        userDataPath = ".";
    } else {
        userDataPath = std::string(homePath) + "/Library/Application Support/Lugaru";
    }
#else // Linux
    userDataPath = getGenericDirectory("XDG_DATA_HOME", ".local/share");
#endif
    makeDirectory(userDataPath);
    return userDataPath;
}

std::string Folders::getUserSavePath()
{
    std::string userSavePath = getUserDataPath() + "/saves";
    makeDirectory(userSavePath);  // Ensure the 'saves' directory exists
    return userSavePath;
}

std::string Folders::getConfigFilePath()
{
    std::string configFolder;
#if defined(_WIN32) || (defined(__APPLE__) && defined(__MACH__))
    configFolder = getUserDataPath();
#else // Linux
    configFolder = getGenericDirectory("XDG_CONFIG_HOME", ".config");
    makeDirectory(configFolder);
#endif
    return configFolder + "/config.txt";
}

void Folders::createPackListFile() {
    std::string packListPath = dataDir + "/PackList.json";

    if (file_exists(packListPath)) {
        updatePackList();  // Update existing list
        return;
    }

    Json::Value packListJson;
    packListJson["Packs"]["Mods"] = Json::arrayValue;
    packListJson["Packs"]["TexturePacks"] = Json::arrayValue;  // Add array for texture packs

    // Iterate through each folder in the Data directory
    std::vector<std::string> directories = list_directory(dataDir);
    for (const auto& dir_name : directories) {
        std::string full_dir_path = join_paths(dataDir, dir_name);
        if (is_directory(full_dir_path)) {
            std::string packInfoPath = full_dir_path + "/PackInfo.json";

            if (file_exists(packInfoPath)) {
                std::ifstream packInfoFile(packInfoPath, std::ifstream::binary);
                Json::Value packInfoJson;
                packInfoFile >> packInfoJson;

                Json::Value newPack;
                newPack["ModName"] = packInfoJson.get("Name", "Unknown Pack").asString();
                newPack["Description"] = packInfoJson.get("Description", "No description available.").asString();
                newPack["Version"] = packInfoJson.get("Version", "1.0").asString();
                newPack["Author"] = packInfoJson.get("Author", "Unknown").asString();
                newPack["PackType"] = packInfoJson.get("PackType", "Mod").asString();
                newPack["Status"] = "Enabled";  // Enabled by default

                // Add to correct section (Mods or TexturePacks)
                if (newPack["PackType"].asString() == "Texture") {
                    packListJson["Packs"]["TexturePacks"].append(newPack);
                } else {
                    packListJson["Packs"]["Mods"].append(newPack);
                }
            }
        }
    }

    // Write the updated PackList.json
    std::ofstream packListFile(packListPath);
    Json::StreamWriterBuilder writer;
    packListFile << Json::writeString(writer, packListJson);
}

void Folders::updatePackList() {
    std::string packListPath = dataDir + "/PackList.json";

    // Read the existing PackList.json
    Json::Value packListJson;
    std::ifstream packListFile(packListPath, std::ifstream::binary);
    if (packListFile.is_open()) {
        packListFile >> packListJson;
        packListFile.close();
    } else {
        std::cerr << "Unable to open PackList.json." << std::endl;
        return;
    }

    std::unordered_set<std::string> existingMods;
    std::unordered_set<std::string> existingTexturePacks;

    for (const auto& mod : packListJson["Packs"]["Mods"]) {
        existingMods.insert(mod["ModName"].asString());
    }

    for (const auto& texturePack : packListJson["Packs"]["TexturePacks"]) {
        existingTexturePacks.insert(texturePack["ModName"].asString());
    }

    bool packListUpdated = false;

    // Iterate through each folder in the Data directory
    std::vector<std::string> directories = list_directory(dataDir);
    for (const auto& dir_name : directories) {
        std::string full_dir_path = join_paths(dataDir, dir_name);
        if (is_directory(full_dir_path)) {
            std::string packInfoPath = full_dir_path + "/PackInfo.json";

            if (file_exists(packInfoPath)) {
                std::ifstream packInfoFile(packInfoPath, std::ifstream::binary);
                Json::Value packInfoJson;
                packInfoFile >> packInfoJson;

                std::string packType = packInfoJson.get("PackType", "Mod").asString();

                if (packType == "Mod" && existingMods.find(dir_name) == existingMods.end()) {
                    Json::Value newMod;
                    newMod["ModName"] = packInfoJson.get("Name", dir_name).asString();  // Fallback to folder name
                    newMod["Description"] = packInfoJson.get("Description", "No description available.").asString();
                    newMod["Version"] = packInfoJson.get("Version", "1.0").asString();
                    newMod["Author"] = packInfoJson.get("Author", "Unknown").asString();
                    newMod["PackType"] = "Mod";
                    newMod["Status"] = "Enabled";

                    packListJson["Packs"]["Mods"].append(newMod);
                    packListUpdated = true;
                } else if (packType == "Texture" && existingTexturePacks.find(dir_name) == existingTexturePacks.end()) {
                    Json::Value newTexturePack;
                    newTexturePack["ModName"] = packInfoJson.get("Name", dir_name).asString();
                    newTexturePack["Description"] = packInfoJson.get("Description", "No description available.").asString();
                    newTexturePack["Version"] = packInfoJson.get("Version", "1.0").asString();
                    newTexturePack["Author"] = packInfoJson.get("Author", "Unknown").asString();
                    newTexturePack["PackType"] = "Texture";
                    newTexturePack["Status"] = "Enabled";

                    packListJson["Packs"]["TexturePacks"].append(newTexturePack);
                    packListUpdated = true;
                }
            }
        }
    }

    // Write the updated PackList.json if any changes were made
    if (packListUpdated) {
        std::ofstream outFile(packListPath);
        Json::StreamWriterBuilder writer;
        outFile << Json::writeString(writer, packListJson);
        std::cout << "PackList.json updated successfully." << std::endl;
    }
}

std::string Folders::getResourcePath(const std::string& relativePath) {

    // Remove any occurrence of "Data/" from the relative path
    std::string correctedRelativePath = relativePath;
    size_t pos;
    while ((pos = correctedRelativePath.find("Data/")) != std::string::npos) {
        correctedRelativePath.erase(pos, 5);
    }

    std::string packListPath = dataDir + "/PackList.json";

    // Read PackList.json to get the list of mods and texture packs
    Json::Value packListJson;
    std::ifstream packListFile(packListPath, std::ifstream::binary);
    if (!packListFile.is_open()) {
        std::cerr << "Unable to open PackList.json." << std::endl;
        return "";
    }
    packListFile >> packListJson;
    packListFile.close();

    // Check texture packs first
    const Json::Value& texturePacks = packListJson["Packs"]["TexturePacks"];
    for (const auto& texturePack : texturePacks) {
        if (texturePack["Status"].asString() == "Enabled") {
            std::string packName = texturePack["ModName"].asString();
            std::string texturePackPath = dataDir + "/" + packName + "/" + correctedRelativePath;

            std::string foundFile = findFileCaseInsensitive(texturePackPath);
            if (!foundFile.empty()) {
                return foundFile;  // Return the path if a texture pack contains the file
            }
        }
    }

    // Check mods if not found in texture packs
    const Json::Value& mods = packListJson["Packs"]["Mods"];

    // Check if correctedRelativePath starts with a pack name
    for (const auto& mod : mods) {
        std::string modName = mod["ModName"].asString();

        // If correctedRelativePath starts with modName, search directly in that mod
        if (correctedRelativePath.find(modName + "/") == 0) {
            std::string specificModPath = dataDir + "/" + correctedRelativePath;

            // Check if the file exists using case-insensitive search
            std::string foundFile = findFileCaseInsensitive(specificModPath);
            if (!foundFile.empty()) {
                return foundFile; // Return if found directly in the specific mod path
            } else {
                std::cerr << "File not found in specified mod: " << modName << std::endl;
                return ""; // Return empty if the file is not found in the specified mod
            }
        }
    }

    for (const auto& mod : mods) {
        if (mod["Status"].asString() == "Enabled") {
            std::string modName = mod["ModName"].asString();
            std::string modFolderPath = dataDir + "/" + modName + "/" + correctedRelativePath;

            std::string foundFile = findFileCaseInsensitive(modFolderPath);
            if (!foundFile.empty()) {
                return foundFile;
            }
        }
    }

    // Check base game folders in Data
    std::string baseGamePath = dataDir + "/" + correctedRelativePath;
    std::string foundFile = findFileCaseInsensitive(baseGamePath);
    if (!foundFile.empty()) {
        return foundFile;
    }

    std::cerr << "Resource not found in any path." << std::endl;
    return "";
}

std::string Folders::toLower(const std::string& str) {
    std::string lowerStr = str;
    std::transform(lowerStr.begin(), lowerStr.end(), lowerStr.begin(), ::tolower);
    return lowerStr;
}

std::string Folders::findFileCaseInsensitive(const std::string& path) {
    std::string directory = path.substr(0, path.find_last_of("/\\"));
    std::string requestedFilename = path.substr(path.find_last_of("/\\") + 1);

    if (!file_exists(directory)) {
        return "";
    }

    // Open directory and search for the file
    std::vector<std::string> files = list_directory(directory);
    std::string requestedFilenameLower = toLower(requestedFilename);
    for (const auto& currentFilename : files) {
        if (toLower(currentFilename) == requestedFilenameLower) {
            return directory + "/" + currentFilename;
        }
    }

    // If no match is found, return an empty string
    return "";
}

#ifdef _WIN32

std::vector<std::string> list_directory(const std::string& dir_path) {
    std::vector<std::string> files;
    WIN32_FIND_DATA file_data;
    HANDLE hFind = FindFirstFile((dir_path + "\\*").c_str(), &file_data);

    if (hFind != INVALID_HANDLE_VALUE) {
        do {
            std::string file_name = file_data.cFileName;
            if (file_name != "." && file_name != "..") {
                files.push_back(file_name);
            }
        } while (FindNextFile(hFind, &file_data) != 0);
        FindClose(hFind);
    } else {
        std::cerr << "Could not open directory: " << dir_path << std::endl;
    }
    return files;
}
#endif

#if PLATFORM_UNIX
std::vector<std::string> list_directory(const std::string& dir_path) {
    std::vector<std::string> files;
    DIR* dir = opendir(dir_path.c_str());
    if (dir) {
        struct dirent* entry;
        while ((entry = readdir(dir)) != nullptr) {
            std::string file_name = entry->d_name;
            if (file_name != "." && file_name != "..") {
                files.push_back(file_name);
            }
        }
        closedir(dir);
    } else {
        std::cerr << "Could not open directory: " << dir_path << std::endl;
    }
    return files;
}
#endif

#if PLATFORM_LINUX
/* Generic code for XDG ENVVAR test and fallback */
std::string Folders::getGenericDirectory(const char* ENVVAR, const std::string& fallback)
{
    const char* path = getenv(ENVVAR);
    std::string ret;
    if ((path != NULL) && (strlen(path) != 0)) {
        ret = std::string(path) + "/lugaru";
    } else {
        const char* homedir = getHomeDirectory();
        if ((homedir != NULL) && (strlen(homedir) != 0)) {
            ret = std::string(homedir) + '/' + fallback + "/lugaru";
        } else {
            ret = ".";
        }
    }
    return ret;
}
#endif

#if PLATFORM_UNIX
const char* Folders::getHomeDirectory()
{
    const char* homedir = getenv("HOME");
    if (homedir != NULL) {
        return homedir;
    }
    struct passwd* pw = getpwuid(getuid());
    if (pw != NULL) {
        return pw->pw_dir;
    }
    return NULL;
}
#endif

bool Folders::makeDirectory(const std::string& path)
{
#ifdef _WIN32
    int status = CreateDirectory(path.c_str(), NULL);
    return ((status != 0) || (GetLastError() == ERROR_ALREADY_EXISTS));
#else
    errno = 0;
    int status = mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    return ((status == 0) || (errno == EEXIST));
#endif
}

FILE* Folders::openMandatoryFile(const std::string& filename, const char* mode)
{
    FILE* tfile = fopen(filename.c_str(), mode);
    if (tfile == NULL) {
        throw FileNotFoundException(filename);
    }
    return tfile;
}

bool Folders::file_exists(const std::string& filepath)
{
    FILE* file;
    file = fopen(filepath.c_str(), "rb");
    if (file == NULL) {
        return false;
    } else {
        fclose(file);
        return true;
    }
}

std::string join_paths(const std::string& base, const std::string& filename) {
#ifdef _WIN32
    return base + "\\" + filename;
#else
    return base + "/" + filename;
#endif
}

bool is_directory(const std::string& path) {
    struct stat sb;

#ifdef _WIN32
    // Use _stat for Windows and check for directory flag
    if (_stat(path.c_str(), &sb) == 0 && (sb.st_mode & S_IFDIR)) {
        return true;
    }
#else
    // Use stat for Unix-like systems
    if (stat(path.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)) {
        return true;
    }
#endif

    return false;
}