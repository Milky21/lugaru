/*
Copyright (C) 2003, 2010 - Wolfire Games
Copyright (C) 2010-2017 - Lugaru contributors (see AUTHORS file)

This file is part of Lugaru.

Lugaru is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Lugaru is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lugaru.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _FOLDERS_HPP_
#define _FOLDERS_HPP_

#include <string>
#include <vector>

#ifndef DATA_DIR
#define DATA_DIR "Data"
#endif

struct FileNotFoundException : public std::exception
{
    std::string errorText;

    FileNotFoundException(const std::string& filename)
        : errorText(filename + " could not be found") {}

    const char* what() const throw() { return errorText.c_str(); }
};

class Folders
{
    static const std::string dataDir;

public:
    /** Returns path to the screenshot directory. Creates it if needed. */
    static std::string getScreenshotDir();

    /** Returns full path for user data */
    static std::string getUserDataPath();

    /** Returns full path for config file */
    static std::string getConfigFilePath();

    /** Generate PackList.json if not found and update it if needed */
    static void createPackListFile();

    /**  Update existing PackList.json by adding new packs and optionally removing old ones */
    static void updatePackList();

    /** Opens a mandatory file or throws a FileNotFoundException */
    static FILE* openMandatoryFile(const std::string& filename, const char* mode);

    /** Checks if a file exists */
    static bool file_exists(const std::string& filepath);

    /* Returns full path for a game resource */
    static std::string getResourcePath(const std::string& relativePath);
    
    /** Returns full path for user progress save */
    static std::string getUserSavePath();

    /** Create a directory if it doesn't exist */
    static bool makeDirectory(const std::string& path);

    /** Find a file using case-insensitive search */
    static std::string findFileCaseInsensitive(const std::string& path);

private:
    /** Convert a string to lowercase */
    static std::string toLower(const std::string& str);

    /** Returns the user's home directory */
    static const char* getHomeDirectory();

    /** Get a generic directory path from an environment variable */
    static std::string getGenericDirectory(const char* ENVVAR, const std::string& fallback);
};

/** Utility functions to support directory listing and file operations */
std::vector<std::string> list_directory(const std::string& dir_path);

/** Joins two paths into one */
std::string join_paths(const std::string& base, const std::string& filename);

/** Check if a path is a directory */
bool is_directory(const std::string& path);

#endif /* _FOLDERS_HPP_ */
