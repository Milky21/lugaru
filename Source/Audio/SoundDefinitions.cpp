#include "SoundDefinitions.hpp"

// Define the soundDefinitions map
const std::map<SoundID, SoundInfo> soundDefinitions = {
    { SoundID::FootstepSnow1, { "FootstepSnow1", "FootStepSnow1.ogg", true } },
    { SoundID::FootstepSnow2, { "FootstepSnow2", "FootStepSnow2.ogg", true } },
    { SoundID::FootstepStone1, { "FootstepStone1", "FootStepStone1.ogg", true } },
    { SoundID::FootstepStone2, { "FootstepStone2", "FootStepStone2.ogg", true } },
    { SoundID::FootstepGrass1, { "FootstepGrass1", "FootStepGrass1.ogg", true } },
    { SoundID::FootstepGrass2, { "FootstepGrass2", "FootStepGrass2.ogg", true } },
    { SoundID::Land, { "Land", "Land.ogg", true } },
    { SoundID::Jump, { "Jump", "Jump.ogg", true } },
    { SoundID::Hawk, { "Hawk", "Hawk.ogg", true } },
    { SoundID::Whoosh, { "Whoosh", "Whoosh.ogg", true } },
    { SoundID::Land1, { "Land1", "Land1.ogg", true } },
    { SoundID::Land2, { "Land2", "Land2.ogg", true } },
    { SoundID::Break, { "Break", "Broken.ogg", false } }, // 2D
    { SoundID::LowWhoosh, { "LowWhoosh", "LowWhoosh.ogg", true } },
    { SoundID::MidWhoosh, { "MidWhoosh", "MidWhoosh.ogg", true } },
    { SoundID::HighWhoosh, { "HighWhoosh", "HighWhoosh.ogg", true } },
    { SoundID::MoveWhoosh, { "MoveWhoosh", "MoveWhoosh.ogg", true } },
    { SoundID::HeavyImpact, { "HeavyImpact", "HeavyImpact.ogg", true } },
    { SoundID::WhooshHit, { "WhooshHit", "WhooshHit.ogg", true } },
    { SoundID::Thud, { "Thud", "Thud.ogg", true } },
    { SoundID::Alarm, { "Alarm", "Alarm.ogg", false } }, // 2D
    { SoundID::Break2, { "Break2", "Break.ogg", false } }, // 2D
    { SoundID::KnifeDraw, { "KnifeDraw", "KnifeDraw.ogg", true } },
    { SoundID::KnifeSheathe, { "KnifeSheathe", "KnifeSheathe.ogg", true } },
    { SoundID::FleshStab, { "FleshStab", "FleshStab.ogg", true } },
    { SoundID::FleshStabRemove, { "FleshStabRemove", "FleshStabRemove.ogg", true } },
    { SoundID::KnifeSwish, { "KnifeSwish", "KnifeSwish.ogg", true } },
    { SoundID::KnifeSlice, { "KnifeSlice", "KnifeSlice.ogg", true } },
    { SoundID::SwordSlice, { "SwordSlice", "SwordSlice.ogg", true } },
    { SoundID::Skid, { "Skid", "Skid.ogg", true } },
    { SoundID::SnowSkid, { "SnowSkid", "SnowSkid.ogg", true } },
    { SoundID::BushRustle, { "BushRustle", "BushRustle.ogg", true } },
    { SoundID::Clank1, { "Clank1", "Clank1.ogg", true } },
    { SoundID::Clank2, { "Clank2", "Clank2.ogg", true } },
    { SoundID::Clank3, { "Clank3", "Clank3.ogg", true } },
    { SoundID::Clank4, { "Clank4", "Clank4.ogg", true } },
    { SoundID::ConsoleSuccess, { "ConsoleSuccess", "ConsoleSuccess.ogg", false } }, // 2D
    { SoundID::ConsoleFail, { "ConsoleFail", "ConsoleFail.ogg", false } }, // 2D
    { SoundID::MetalHit, { "MetalHit", "MetalHit.ogg", true } },
    { SoundID::ClawSlice, { "ClawSlice", "ClawSlice.ogg", true } },
    { SoundID::Splatter, { "Splatter", "Splatter.ogg", true } },
    { SoundID::Growl, { "Growl", "Growl.ogg", true } },
    { SoundID::Growl2, { "Growl2", "Growl2.ogg", true } },
    { SoundID::Bark, { "Bark", "Bark.ogg", true } },
    { SoundID::Bark2, { "Bark2", "Bark2.ogg", true } },
    { SoundID::Bark3, { "Bark3", "Bark3.ogg", true } },
    { SoundID::Snarl, { "Snarl", "Snarl.ogg", true } },
    { SoundID::Snarl2, { "Snarl2", "Snarl2.ogg", true } },
    { SoundID::BarkGrowl, { "BarkGrowl", "BarkGrowl.ogg", true } },
    { SoundID::RabbitAttack, { "RabbitAttack", "RabbitAttack.ogg", true } },
    { SoundID::RabbitAttack2, { "RabbitAttack2", "RabbitAttack2.ogg", true } },
    { SoundID::RabbitAttack3, { "RabbitAttack3", "RabbitAttack3.ogg", true } },
    { SoundID::RabbitAttack4, { "RabbitAttack4", "RabbitAttack4.ogg", true } },
    { SoundID::RabbitPain, { "RabbitPain", "RabbitPain.ogg", true } },
    { SoundID::RabbitPain2, { "RabbitPain2", "RabbitPain2.ogg", true } },
    { SoundID::RabbitChitter, { "RabbitChitter", "RabbitChitter.ogg", true } },
    { SoundID::RabbitChitter2, { "RabbitChitter2", "RabbitChitter2.ogg", true } },
    { SoundID::SwordStaff, { "SwordStaff", "SwordStaff.ogg", true } },
    { SoundID::StaffBody, { "StaffBody", "StaffBody.ogg", true } },
    { SoundID::StaffHead, { "StaffHead", "StaffHead.ogg", true } },
    { SoundID::StaffBreak, { "StaffBreak", "StaffBreak.ogg", true } },
    { SoundID::Fire, { "Fire", "Fire.ogg", false } },
    { SoundID::FireStart, { "FireStart", "FireStart.ogg", false } }, // 2D
    { SoundID::FireEnd, { "FireEnd", "FireEnd.ogg", false } }, // 2D
    { SoundID::Wind, { "Wind", "Wind.ogg", false } },
    { SoundID::DesertAmbient, { "DesertAmbient", "DesertAmbient.ogg", false } },
    { SoundID::Count, { "Count", "", false } } // Placeholder, not used
};

// Define the trackDefinitions map
const std::map<TrackID, TrackInfo> trackDefinitions = {
    { TrackID::GrassTheme, { "GrassTheme", "Music1Grass.ogg" } },
    { TrackID::SnowTheme, { "SnowTheme", "Music1Snow.ogg" } },
    { TrackID::DesertTheme, { "DesertTheme", "Music1Desert.ogg" } },
    { TrackID::FightTheme, { "FightTheme", "Music2.ogg" } },
    { TrackID::MenuTheme, { "MenuTheme", "Music3.ogg" } },
    { TrackID::Count, { "Count", "" } } // Placeholder, not used
};
