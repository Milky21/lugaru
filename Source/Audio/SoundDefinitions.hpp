#ifndef SOUNDDEFINITIONS_HPP
#define SOUNDDEFINITIONS_HPP

#include <string>
#include <map>

// Enumerations for sound effects
enum class SoundID {
    FootstepSnow1,
    FootstepSnow2,
    FootstepStone1,
    FootstepStone2,
    FootstepGrass1,
    FootstepGrass2,
    Land,
    Jump,
    Hawk,
    Whoosh,
    Land1,
    Land2,
    Break,
    LowWhoosh,
    MidWhoosh,
    HighWhoosh,
    MoveWhoosh,
    HeavyImpact,
    WhooshHit,
    Thud,
    Alarm,
    Break2,
    KnifeDraw,
    KnifeSheathe,
    FleshStab,
    FleshStabRemove,
    KnifeSwish,
    KnifeSlice,
    SwordSlice,
    Skid,
    SnowSkid,
    BushRustle,
    Clank1,
    Clank2,
    Clank3,
    Clank4,
    ConsoleSuccess,
    ConsoleFail,
    MetalHit,
    ClawSlice,
    Splatter,
    Growl,
    Growl2,
    Bark,
    Bark2,
    Bark3,
    Snarl,
    Snarl2,
    BarkGrowl,
    RabbitAttack,
    RabbitAttack2,
    RabbitAttack3,
    RabbitAttack4,
    RabbitPain,
    RabbitPain2,
    RabbitChitter,
    RabbitChitter2,
    SwordStaff,
    StaffBody,
    StaffHead,
    StaffBreak,
    Fire,
    FireStart,
    FireEnd,
    Wind,
    DesertAmbient,
    Count // Keeps track of the number of sounds
};

// Enumerations for music tracks
enum class TrackID {
    GrassTheme,
    SnowTheme,
    DesertTheme,
    FightTheme,
    MenuTheme,
    Count // Keeps track of the number of tracks
};

// Structure to hold sound information
struct SoundInfo {
    std::string name;
    std::string filename;
    bool is3D; // true for 3D sounds, false for 2D sounds
};

// Structure to hold track information
struct TrackInfo {
    std::string name;
    std::string filename;
};

// Map of sound definitions
extern const std::map<SoundID, SoundInfo> soundDefinitions;

// Map of track definitions
extern const std::map<TrackID, TrackInfo> trackDefinitions;

#endif // SOUNDDEFINITIONS_HPP
