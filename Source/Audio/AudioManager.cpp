#include "AudioManager.hpp"
#include "Utils/Folders.hpp"
#include <cmath> // for sqrt
#include <cstring>
#include <iostream>
#include <map>
#include <vector>
#include <vorbis/vorbisfile.h>

// External variables (assuming they're defined elsewhere)
extern float slomofreq;
extern XYZ envsound[30];
extern float envsoundvol[30];
extern int numenvsounds;
extern float envsoundlife[30];

/**
 * @brief Adjust this limit or use OpenAL's distance attenuation
 *        to avoid manually culling sounds.
 */
#define MAX_SOUND_DISTANCE 10.0f

#ifndef LOG
#include <iostream>
#define LOG(msg) std::cout << "[AudioManager] " << msg << std::endl
#endif

// --- Static Member Initialization ---
ALCdevice* AudioManager::device = nullptr;
ALCcontext* AudioManager::context = nullptr;
std::map<SoundID, AudioManager::Sound> AudioManager::sounds;
std::map<SoundID, AudioManager::SourceData> AudioManager::playingSounds;
std::map<TrackID, ALuint> AudioManager::tracks;

ALuint AudioManager::currentTrackSource = 0;
TrackID AudioManager::currentTrackID = TrackID::Count;
float AudioManager::soundEffectsVolume = 1.0f;
float AudioManager::musicVolume = 1.0f;

SoundID AudioManager::footstepsound = SoundID::FootstepSnow1;
SoundID AudioManager::footstepsound2 = SoundID::FootstepSnow2;
SoundID AudioManager::footstepsound3 = SoundID::FootstepStone1;
SoundID AudioManager::footstepsound4 = SoundID::FootstepStone2;

XYZ AudioManager::listenerPosition(0.0f, 0.0f, 0.0f);

/**
 * @brief Helper function to see how many sources can be created.
 *        This is purely diagnostic and not strictly necessary.
 */
static int getMaxSources()
{
    ALuint source;
    int sourceCount = 0;
    ALenum error;
    const int maxSourceLimit = 256; // Add a safety limit

    while (sourceCount < maxSourceLimit) {
        alGenSources(1, &source);
        error = alGetError();
        if (error != AL_NO_ERROR) {
            break; // Stop when no more sources can be generated
        }
        sourceCount++;
        alDeleteSources(1, &source);
    }

    return sourceCount;
}

// ------------------------------------------------------------------
// Public Methods
// ------------------------------------------------------------------

bool AudioManager::initialize()
{
    LOG("Initializing AudioManager...");

    // Open audio device
    device = alcOpenDevice(nullptr);
    if (!device) {
        LOG("Failed to open audio device.");
        return false;
    }

    // Create audio context
    context = alcCreateContext(device, nullptr);
    if (!context) {
        LOG("Failed to create audio context.");
        alcCloseDevice(device);
        device = nullptr;
        return false;
    }

    // Make context current
    if (!alcMakeContextCurrent(context)) {
        LOG("Failed to make audio context current.");
        alcDestroyContext(context);
        context = nullptr;
        alcCloseDevice(device);
        device = nullptr;
        return false;
    }

    // Set the distance model
    alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);

    // Log the max number of sources (channels)
    int maxSources = getMaxSources();
    LOG("Max available sound sources: " + std::to_string(maxSources));

    // Set default listener properties
    XYZ listenerPos{ 0.0f, 0.0f, 0.0f };
    setListenerPosition(listenerPos);

    XYZ listenerAt{ 0.0f, 0.0f, -1.0f };
    XYZ listenerUp{ 0.0f, 1.0f, 0.0f };
    setListenerOrientation(listenerAt, listenerUp);

    // Load sounds and tracks
    loadAllSounds();
    LOG("All sounds loaded successfully.");

    loadAllTracks();
    LOG("All tracks loaded successfully.");

    return true;
}

void AudioManager::shutdown()
{
    LOG("Shutting down AudioManager...");

    // Stop and delete current track
    if (currentTrackSource != 0) {
        alSourceStop(currentTrackSource);
        alDeleteSources(1, &currentTrackSource);
        currentTrackSource = 0;
        currentTrackID = TrackID::Count;
    }

    // Stop all playing sounds and delete sources
    for (auto& pair : playingSounds) {
        alSourceStop(pair.second.source);
        alDeleteSources(1, &pair.second.source);
    }
    playingSounds.clear();

    // Delete sound buffers
    for (auto& pair : sounds) {
        alDeleteBuffers(1, &pair.second.buffer);
    }
    sounds.clear();

    // Delete track buffers
    for (auto& pair : tracks) {
        alDeleteBuffers(1, &pair.second);
    }
    tracks.clear();

    // Destroy context and close device
    if (context) {
        alcMakeContextCurrent(nullptr);
        alcDestroyContext(context);
        context = nullptr;
    }

    if (device) {
        alcCloseDevice(device);
        device = nullptr;
    }
}

static void* decodeOggToPCM(const std::string& filename, ALenum& format, ALsizei& size, ALuint& freq)
{
    FILE* file = fopen(filename.c_str(), "rb");
    if (!file) {
        LOG("Failed to open audio file: " + filename);
        return nullptr;
    }

    OggVorbis_File vf;
    if (ov_open(file, &vf, nullptr, 0) < 0) {
        fclose(file);
        LOG("Failed to open Ogg Vorbis file: " + filename);
        return nullptr;
    }

    vorbis_info* info = ov_info(&vf, -1);
    freq = info->rate;
    format = (info->channels == 1) ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16;

    size_t totalSize = 0;
    const size_t bufferSize = 4096;
    std::vector<char> buffer;
    int bitstream = 0;
    char tempArray[4096];
    long bytesRead = 0;

    while ((bytesRead = ov_read(&vf, tempArray, static_cast<int>(bufferSize), 0, 2, 1, &bitstream)) > 0) {
        buffer.insert(buffer.end(), tempArray, tempArray + bytesRead);
        totalSize += bytesRead;
    }

    ov_clear(&vf);

    size = static_cast<ALsizei>(totalSize);
    if (size == 0) {
        LOG("Ogg file read zero bytes: " + filename);
        return nullptr;
    }

    // Allocate raw PCM data
    char* data = new char[size];
    std::memcpy(data, buffer.data(), size);

    return data;
}

bool AudioManager::loadAudioFile(const std::string& filename, ALuint* buffer)
{
    ALenum format = 0;
    ALsizei size = 0;
    ALuint freq = 0;

    void* data = decodeOggToPCM(filename, format, size, freq);
    if (!data) {
        return false;
    }

    alGenBuffers(1, buffer);
    ALenum error = alGetError();
    if (error != AL_NO_ERROR) {
        LOG("Error generating AL buffer. Error code: " + std::to_string(error));
        delete[] static_cast<char*>(data);
        return false;
    }

    alBufferData(*buffer, format, data, size, freq);
    delete[] static_cast<char*>(data);

    error = alGetError();
    if (error != AL_NO_ERROR) {
        LOG("Error buffering data. Error code: " + std::to_string(error));
        return false;
    }

    return true;
}

void AudioManager::loadAllSounds()
{
    LOG("Loading all sounds...");

    for (const auto& pair : soundDefinitions) {
        SoundID soundID = pair.first;
        const SoundInfo& info = pair.second;

        if (sounds.count(soundID) > 0) {
            LOG("Sound already loaded: " + info.name);
            continue;
        }

        std::string soundPath = "Sounds/" + info.filename;
        std::string fullPath = Folders::getResourcePath(soundPath);

        if (fullPath.empty()) {
            LOG("Failed to find sound file in resource path: " + soundPath);
            continue;
        }

        ALuint buffer;
        if (!loadAudioFile(fullPath, &buffer)) {
            LOG("Failed to load sound: " + info.filename);
            continue;
        }

        sounds[soundID] = { buffer, info.is3D };
        LOG("Loaded sound: " + info.name + " from file: " + info.filename);
    }
}

void AudioManager::loadAllTracks()
{
    LOG("Loading all music tracks...");

    for (const auto& pair : trackDefinitions) {
        TrackID trackID = pair.first;
        const TrackInfo& info = pair.second;

        if (tracks.count(trackID) > 0) {
            LOG("Track already loaded: " + info.name);
            continue;
        }

        std::string trackPath = "Sounds/" + info.filename;
        std::string fullPath = Folders::getResourcePath(trackPath);

        if (fullPath.empty()) {
            LOG("Failed to find music track file in resource path: " + trackPath);
            continue;
        }

        ALuint buffer;
        if (!loadAudioFile(fullPath, &buffer)) {
            LOG("Failed to load track: " + info.filename);
            continue;
        }

        tracks[trackID] = buffer;
        LOG("Loaded track: " + info.name + " from file: " + info.filename);
    }
}

void AudioManager::playSound(SoundID soundID, const XYZ& position, float volume)
{
    if (sounds.count(soundID) == 0) {
        LOG("Sound not loaded: " + std::to_string(static_cast<int>(soundID)));
        return;
    }

    const Sound& sound = sounds[soundID];
    float finalVolume = volume * soundEffectsVolume; // Apply SFX master volume

    // Optional manual distance check (could rely on OpenAL's distance attenuation)
    if (sound.is3D) {
        float dx = position.x - listenerPosition.x;
        float dy = position.y - listenerPosition.y;
        float dz = position.z - listenerPosition.z;
        float distance = std::sqrt(dx * dx + dy * dy + dz * dz);

        if (distance > MAX_SOUND_DISTANCE) {
            // Early out if beyond max distance
            return;
        }
    }

    // Reuse the single source if it exists, unless it's stopped
    if (playingSounds.count(soundID) > 0) {
        ALuint source = playingSounds[soundID].source;
        ALint state = 0;
        alGetSourcei(source, AL_SOURCE_STATE, &state);

        if (state == AL_PLAYING || state == AL_PAUSED) {
            // Already playing; do not restart
            return;
        } else {
            // If it was stopped, delete and remove
            alDeleteSources(1, &source);
            playingSounds.erase(soundID);
        }
    }

    ALuint source = 0;
    alGenSources(1, &source);
    if (alGetError() != AL_NO_ERROR) {
        LOG("Error generating source for soundID " + std::to_string(static_cast<int>(soundID)));
        return;
    }

    alSourcei(source, AL_BUFFER, sound.buffer);
    alSourcef(source, AL_GAIN, finalVolume);

    // Set 3D or 2D position
    if (sound.is3D) {
        alSourcei(source, AL_SOURCE_RELATIVE, AL_FALSE);
        alSource3f(source, AL_POSITION, position.x, position.y, -position.z);
    } else {
        alSourcei(source, AL_SOURCE_RELATIVE, AL_TRUE);
        alSource3f(source, AL_POSITION, 0.0f, 0.0f, 0.0f);
    }

    alSourcePlay(source);
    playingSounds[soundID] = { source };
}

void AudioManager::playLoopedSound(SoundID soundID, const XYZ& position, float volume)
{
    if (sounds.count(soundID) == 0) {
        LOG("Sound not loaded: " + std::to_string(static_cast<int>(soundID)));
        return;
    }

    const Sound& sound = sounds[soundID];

    // Manual distance check
    if (sound.is3D) {
        float dx = position.x - listenerPosition.x;
        float dy = position.y - listenerPosition.y;
        float dz = position.z - listenerPosition.z;
        float distance = std::sqrt(dx * dx + dy * dy + dz * dz);

        if (distance > MAX_SOUND_DISTANCE) {
            return;
        }
    }

    // If already playing, let it continue
    if (playingSounds.count(soundID) > 0) {
        ALuint existingSource = playingSounds[soundID].source;
        ALint state = 0;
        alGetSourcei(existingSource, AL_SOURCE_STATE, &state);

        // If it's already playing or paused, do nothing
        if (state == AL_PLAYING || state == AL_PAUSED) {
            return;
        } else {
            // If it was stopped, remove it before re-creating
            alDeleteSources(1, &existingSource);
            playingSounds.erase(soundID);
        }
    }

    ALuint source = 0;
    alGenSources(1, &source);
    if (alGetError() != AL_NO_ERROR) {
        LOG("Error generating source for looped soundID " + std::to_string(static_cast<int>(soundID)));
        return;
    }

    alSourcei(source, AL_BUFFER, sound.buffer);
    alSourcef(source, AL_GAIN, volume);
    alSourcei(source, AL_LOOPING, AL_TRUE);

    if (sound.is3D) {
        alSourcei(source, AL_SOURCE_RELATIVE, AL_FALSE);
        alSource3f(source, AL_POSITION, position.x, position.y, -position.z);
    } else {
        alSourcei(source, AL_SOURCE_RELATIVE, AL_TRUE);
        alSource3f(source, AL_POSITION, 0.0f, 0.0f, 0.0f);
    }

    alSourcePlay(source);
    playingSounds[soundID] = { source };
}

void AudioManager::stopSound(SoundID soundID)
{
    if (playingSounds.count(soundID) == 0) {
        return;
    }

    ALuint source = playingSounds[soundID].source;
    alSourceStop(source);
    alDeleteSources(1, &source);
    playingSounds.erase(soundID);
}

void AudioManager::stopAllSounds()
{
    for (auto& pair : playingSounds) {
        alSourceStop(pair.second.source);
        alDeleteSources(1, &pair.second.source);
    }
    playingSounds.clear();
}

void AudioManager::setSoundVolume(SoundID soundID, float volume)
{
    if (playingSounds.count(soundID) == 0) {
        return;
    }
    ALuint source = playingSounds[soundID].source;
    alSourcef(source, AL_GAIN, volume);
}

void AudioManager::changeTrack(TrackID trackID, float volume)
{
    if (tracks.count(trackID) == 0) {
        LOG("Error: Track not loaded. Track ID: " + std::to_string(static_cast<int>(trackID)));
        return;
    }

    // Stop current track if different
    if (currentTrackSource != 0 && currentTrackID != trackID) {
        LOG("Stopping current track before changing...");
        stopTrack();
    }

    // Generate a new source for the track if needed
    ALuint buffer = tracks[trackID];
    alGenSources(1, &currentTrackSource);

    if (alGetError() != AL_NO_ERROR) {
        LOG("Error generating audio source for track.");
        currentTrackSource = 0;
        return;
    }

    // Apply buffer, volume, loop settings
    alSourcei(currentTrackSource, AL_BUFFER, buffer);
    alSourcef(currentTrackSource, AL_GAIN, volume * musicVolume); // Apply music master volume
    alSourcei(currentTrackSource, AL_LOOPING, AL_TRUE);
    alSourcei(currentTrackSource, AL_SOURCE_RELATIVE, AL_TRUE);
    alSource3f(currentTrackSource, AL_POSITION, 0.0f, 0.0f, 0.0f);

    alSourcePlay(currentTrackSource);
    if (alGetError() != AL_NO_ERROR) {
        LOG("Error playing audio source for track.");
        alDeleteSources(1, &currentTrackSource);
        currentTrackSource = 0;
        return;
    }

    currentTrackID = trackID;
    LOG("Now playing track ID " + std::to_string(static_cast<int>(currentTrackID)) +
        " at volume " + std::to_string(volume));
}

void AudioManager::stopTrack()
{
    if (currentTrackSource != 0) {
        LOG("Stopping current track...");
        alSourceStop(currentTrackSource);

        // Ensure the source is fully stopped
        ALint state;
        alGetSourcei(currentTrackSource, AL_SOURCE_STATE, &state);

        // TODO: add a break condition for safety
        // if the hardware never signals AL_STOPPED
        while (state == AL_PLAYING) {
            alGetSourcei(currentTrackSource, AL_SOURCE_STATE, &state);
        }

        alDeleteSources(1, &currentTrackSource);
        currentTrackSource = 0;
        currentTrackID = TrackID::Count;
        LOG("Stopped and deleted current track source.");
    }
}

bool AudioManager::isTrackPlaying(TrackID trackID)
{
    if (currentTrackID != trackID || currentTrackSource == 0) {
        return false;
    }
    ALint state;
    alGetSourcei(currentTrackSource, AL_SOURCE_STATE, &state);
    return (state == AL_PLAYING);
}

void AudioManager::setTrackVolume(float volume)
{
    if (currentTrackSource != 0) {
        alSourcef(currentTrackSource, AL_GAIN, volume);
    }
}

void AudioManager::setSoundEffectsVolume(float volume)
{
    soundEffectsVolume = volume;
    LOG("Sound effects volume set to " + std::to_string(volume));
}

void AudioManager::setMusicVolume(float volume)
{
    musicVolume = volume;
    if (currentTrackSource != 0) {
        alSourcef(currentTrackSource, AL_GAIN, volume);
    }
    LOG("Music volume set to " + std::to_string(volume));
}

void AudioManager::setPitch(SoundID soundID, bool slomo)
{
    if (soundID == SoundID::Count) {
        // Apply pitch change to all playing sounds
        for (auto& pair : playingSounds) {
            setPitchForSource(pair.second.source, slomo);
        }
    } else if (playingSounds.count(soundID) > 0) {
        setPitchForSource(playingSounds[soundID].source, slomo);
    }
}

void AudioManager::setTrackPitch(TrackID trackID, bool slomo)
{
    if (trackID == TrackID::Count || currentTrackID != trackID) {
        return;
    }
    setPitchForSource(currentTrackSource, slomo);
}

void AudioManager::setGlobalPitch(bool slomo)
{
    // Set pitch for all sound sources
    for (auto& pair : playingSounds) {
        setPitchForSource(pair.second.source, slomo);
    }
    // Set pitch for the music track
    if (currentTrackSource != 0) {
        setPitchForSource(currentTrackSource, slomo);
    }
}

void AudioManager::update()
{
    std::vector<SoundID> toRemove;
    toRemove.reserve(playingSounds.size());

    for (auto& pair : playingSounds) {
        ALuint source = pair.second.source;
        ALint state = 0;
        alGetSourcei(source, AL_SOURCE_STATE, &state);

        if (state == AL_STOPPED) {
            alDeleteSources(1, &source);
            toRemove.push_back(pair.first);
        }
    }

    for (SoundID sid : toRemove) {
        playingSounds.erase(sid);
    }
}

void AudioManager::setListenerPosition(const XYZ& position)
{
    listenerPosition = position;
    // NOTE: We invert Z for OpenAL's coordinate system
    alListener3f(AL_POSITION, position.x, position.y, -position.z);
}

void AudioManager::setListenerOrientation(const XYZ& at, const XYZ& up)
{
    float orientation[] = {
        at.x, at.y, -at.z, up.x, up.y, -up.z
    };
    alListenerfv(AL_ORIENTATION, orientation);
}

void AudioManager::addEnvSound(const XYZ& coords, float volume, float life)
{
    if (numenvsounds >= 30) {
        LOG("Maximum number of environmental sounds reached.");
        return;
    }

    envsound[numenvsounds] = coords;
    envsoundvol[numenvsounds] = volume;
    envsoundlife[numenvsounds] = life;
    numenvsounds++;
}

void AudioManager::pauseAudio()
{
    for (auto& pair : playingSounds) {
        alSourcePause(pair.second.source);
    }
    if (currentTrackSource != 0) {
        alSourcePause(currentTrackSource);
    }
}

void AudioManager::resumeAudio()
{
    for (auto& pair : playingSounds) {
        alSourcePlay(pair.second.source);
    }
    if (currentTrackSource != 0) {
        alSourcePlay(currentTrackSource);
    }
}

// ------------------------------------------------------------------
// Private / Internal Methods
// ------------------------------------------------------------------

void AudioManager::setPitchForSource(ALuint source, bool slomo)
{
    // Note: This uses a hard-coded 44100 as a reference.
    if (slomo) {
        alSourcef(source, AL_PITCH, static_cast<ALfloat>(slomofreq) / 44100.0f);
    } else {
        alSourcef(source, AL_PITCH, 1.0f);
    }
}
