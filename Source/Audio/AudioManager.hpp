#ifndef AUDIOMANAGER_HPP
#define AUDIOMANAGER_HPP

#include <map>
#include <string>
#include "Math/XYZ.hpp"
#include "SoundDefinitions.hpp"

// Include OpenAL headers
#ifdef __APPLE__
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#else
#include <AL/al.h>
#include <AL/alc.h>
#endif

/**
 * @brief The AudioManager class is responsible for initializing OpenAL,
 *        loading sound and music buffers, playing/stopping sounds and music,
 *        and managing listener orientation.
 */
class AudioManager
{
public:
    // Initialize and shutdown the audio system
    static bool initialize();
    static void shutdown();

    // Load sounds and tracks
    static void loadAllSounds();
    static void loadAllTracks();

    // Sound IDs for footsteps (as per your usage)
    static SoundID footstepsound;
    static SoundID footstepsound2;
    static SoundID footstepsound3;
    static SoundID footstepsound4;

    /**
     * @brief Play a sound (2D or 3D) once at the given position.
     * @param soundID - ID of the sound to play.
     * @param position - Position in 3D space (XYZ). Default is origin (2D).
     * @param volume - Volume multiplier for this sound instance.
     */
    static void playSound(SoundID soundID, const XYZ& position = XYZ(), float volume = 1.0f);

    /**
     * @brief Play a sound looped (2D or 3D) at the given position until stopped.
     * @param soundID - ID of the sound to loop.
     * @param position - Position in 3D space (XYZ). Default is origin (2D).
     * @param volume - Volume multiplier for this sound instance.
     */
    static void playLoopedSound(SoundID soundID, const XYZ& position = XYZ(), float volume = 1.0f);

    // Stop sounds
    static void stopSound(SoundID soundID);
    static void stopAllSounds();

    // Set volume for a specific sound (if it is currently playing)
    static void setSoundVolume(SoundID soundID, float volume);

    // Music track control
    static void changeTrack(TrackID trackID, float volume = 1.0f);
    static void stopTrack();
    static bool isTrackPlaying(TrackID trackID);

    // Set volume for the current music track
    static void setTrackVolume(float volume);

    // Set master volumes
    static void setSoundEffectsVolume(float volume);  // For adjusting all sound effects volume
    static void setMusicVolume(float volume);         // For adjusting music volume

    // Pitch control
    static void setPitch(SoundID soundID = SoundID::Count, bool slomo = false);
    static void setTrackPitch(TrackID trackID, bool slomo = false);
    static void setGlobalPitch(bool slomo = false);

    // Update the audio system (clean up finished sounds, etc.)
    static void update();

    // Set listener position and orientation
    static void setListenerPosition(const XYZ& position);
    static void setListenerOrientation(const XYZ& at, const XYZ& up);

    // Environmental sound handling
    static void addEnvSound(const XYZ& coords, float volume = 16.0f, float life = 0.4f);

    // Pause and resume audio
    static void pauseAudio();
    static void resumeAudio();

    // Master volume controls (publicly visible)
    static float soundEffectsVolume;  // Master volume for sound effects
    static float musicVolume;         // Master volume for music

private:
    // Private constructor to prevent instantiation
    AudioManager() {}

    // Internal methods and data structures
    static bool loadAudioFile(const std::string& filename, ALuint* buffer);

    // Utility for setting pitch on a single source
    static void setPitchForSource(ALuint source, bool slomo);

    // OpenAL device and context
    static ALCdevice* device;
    static ALCcontext* context;

    // Sound struct to store buffer and 3D property
    struct Sound
    {
        ALuint buffer;
        bool is3D;
    };

    // Tracks currently playing sources
    struct SourceData
    {
        ALuint source;
    };

    // Maps to store sounds and currently playing sources
    static std::map<SoundID, Sound> sounds;
    static std::map<SoundID, SourceData> playingSounds;

    // Music tracks
    static std::map<TrackID, ALuint> tracks;
    static ALuint currentTrackSource;
    static TrackID currentTrackID;

    // Listener position
    static XYZ listenerPosition;
};

#endif
