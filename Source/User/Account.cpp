/*
Copyright (C) 2003, 2010 - Wolfire Games
Copyright (C) 2010-2017 - Lugaru contributors (see AUTHORS file)

This file is part of Lugaru.

Lugaru is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Lugaru is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Lugaru.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "User/Account.hpp"

#include "Platform/Platform.hpp"
#include "Utils/binio.h"

#include "Account.hpp"
#include "Utils/Folders.hpp"
#include <fstream>
#include <iostream>
#include <string.h>
#include <sys/stat.h>
#include <json/json.h>

using namespace std;

extern bool devtools;

vector<Account> Account::accounts;
int Account::i_active = -1;

Account::Account(const string& name)
    : name(name)
    , campaignProgress()
{
    difficulty = 0;
    progress = 0;
    points = 0;
    memset(highscore, 0, sizeof(highscore));
    memset(fasttime, 0, sizeof(fasttime));
    memset(unlocked, 0, sizeof(unlocked));
    memset(flawless, 0, sizeof(flawless));

    setCurrentCampaign("Lugaru");
}

Account::Account(FILE* tfile)
    : Account("")
{
    funpackf(tfile, "Bi", &difficulty);
    funpackf(tfile, "Bi", &progress);
    int nbCampaigns;
    funpackf(tfile, "Bi", &nbCampaigns);

    for (int k = 0; k < nbCampaigns; ++k) {
        string campaignName = "";
        int t;
        char c;
        funpackf(tfile, "Bi", &t);
        for (int j = 0; j < t; j++) {
            funpackf(tfile, "Bb", &c);
            campaignName.append(1, c);
        }
        float fscore, fhighscore;
        funpackf(tfile, "Bf", &(campaignProgress[campaignName].time));
        funpackf(tfile, "Bf", &(fscore));
        funpackf(tfile, "Bf", &(campaignProgress[campaignName].fasttime));
        funpackf(tfile, "Bf", &(fhighscore));
        campaignProgress[campaignName].score = fscore;
        campaignProgress[campaignName].highscore = fhighscore;
        int campaignchoicesmade, campaignchoice;
        funpackf(tfile, "Bi", &campaignchoicesmade);
        for (int j = 0; j < campaignchoicesmade; j++) {
            funpackf(tfile, "Bi", &campaignchoice);
            if (campaignchoice >= 10) { // what is that for?
                campaignchoice = 0;
            }
            campaignProgress[campaignName].choices.push_back(campaignchoice);
        }
    }

    currentCampaign = "";
    int t;
    char c;
    funpackf(tfile, "Bi", &t);
    for (int i = 0; i < t; i++) {
        funpackf(tfile, "Bb", &c);
        currentCampaign.append(1, c);
    }

    funpackf(tfile, "Bf", &points);
    for (int i = 0; i < 50; i++) {
        float fscore;
        funpackf(tfile, "Bf", &(fscore));
        funpackf(tfile, "Bf", &(fasttime[i]));
        highscore[i] = fscore;
    }
    for (int i = 0; i < 60; i++) {
        funpackf(tfile, "Bb", &(unlocked[i]));
    }
    int temp;
    char ctemp;
    funpackf(tfile, "Bi", &temp);
    for (int i = 0; i < temp; i++) {
        funpackf(tfile, "Bb", &ctemp);
        name.append(1, ctemp);
    }
    if (name.empty()) {
        name = "Lugaru Player"; // no empty player name security.
    }
}

void Account::save(FILE* tfile)
{
    if (!tfile) {
        std::cerr << "Error: Invalid file pointer passed to save method." << std::endl;
        return;
    }

    fpackf(tfile, "Bi", difficulty);
    fpackf(tfile, "Bi", progress);
    fpackf(tfile, "Bi", campaignProgress.size());

    for (const auto& [campaignName, progress] : campaignProgress) {
        fpackf(tfile, "Bi", campaignName.size());
        for (char c : campaignName) {
            fpackf(tfile, "Bb", c);
        }
        fpackf(tfile, "Bf", progress.time);
        fpackf(tfile, "Bf", float(progress.score));
        fpackf(tfile, "Bf", progress.fasttime);
        fpackf(tfile, "Bf", float(progress.highscore));
        fpackf(tfile, "Bi", progress.choices.size());
        for (int choice : progress.choices) {
            fpackf(tfile, "Bi", choice);
        }
    }

    fpackf(tfile, "Bi", getCurrentCampaign().size());
    for (char c : getCurrentCampaign()) {
        fpackf(tfile, "Bb", c);
    }

    fpackf(tfile, "Bf", points);
    for (int i = 0; i < 50; i++) {
        fpackf(tfile, "Bf", float(highscore[i]));
        fpackf(tfile, "Bf", fasttime[i]);
    }
    for (int i = 0; i < 60; i++) {
        fpackf(tfile, "Bb", unlocked[i]);
    }

    fpackf(tfile, "Bi", name.size());
    for (char c : name) {
        fpackf(tfile, "Bb", c);
    }

    std::cerr << "Successfully saved account: " << name << std::endl;
}

void Account::setCurrentCampaign(const string& name)
{
    currentCampaign = name;
}

void Account::setName(const std::string& newName) {
    name = newName;
}

void Account::renameAccount(Account& account, const std::string& newName) {
    // Clone the current account with the new name
    Account newAccount = account;
    newAccount.setName(newName);

    // Save the new account
    newAccount.saveFile(newName);

    // Delete the old account file
    std::string oldFilePath = Folders::getUserSavePath() + "/" + account.getName() + ".json";
    remove(oldFilePath.c_str());

    // Set the active account to the new one
    account = newAccount;
    saveActive();
}

void Account::add(const string& name)
{
    accounts.emplace_back(name);
    i_active = accounts.size() - 1;
}

Account& Account::get(int i)
{
    return accounts.at(i);
}

int Account::getNbAccounts()
{
    return accounts.size();
}

bool Account::hasActive()
{
    return (i_active >= 0);
}

Account& Account::active()
{
    return accounts.at(i_active);
}

void Account::setActive(int i)
{
    if ((i >= 0) && (i < int(accounts.size()))) {
        i_active = i;
    } else {
        cerr << "Tried to set active account to " << i << " but there is not such account" << endl;
        i_active = -1;
    }
}

void Account::destroyActive()
{
    if ((i_active >= 0) && (i_active < int(accounts.size()))) {
        accounts.erase(accounts.begin() + i_active);
        i_active = -1;
    } else {
        cerr << "Tried to destroy active account " << i_active << " but there is not such account" << endl;
        i_active = -1;
    }
}

int Account::getDifficulty()
{
    return difficulty;
}

void Account::endGame()
{
    campaignProgress[currentCampaign].choices.clear();
    campaignProgress[currentCampaign].score = 0;
    campaignProgress[currentCampaign].time = 0;
}

void Account::winCampaignLevel(int choice, int score, float time)
{
    campaignProgress[currentCampaign].choices.push_back(choice);
    setCampaignScore(campaignProgress[currentCampaign].score + score);
    campaignProgress[currentCampaign].time = time;
}

void Account::winLevel(int level, int score, float time)
{
    if (!devtools) {
        if (score > highscore[level]) {
            highscore[level] = score;
        }
        if (time < fasttime[level] || fasttime[level] == 0) {
            fasttime[level] = time;
        }
    }
    if (progress < level + 1) {
        progress = level + 1;
    }
}


bool Account::getFlawless(int level) const {
    if (level >= 0 && level < 14) {
        return flawless[level];
    }
    return false;
}

void Account::setFlawless(int level, bool status) {
    if (level >= 0 && level < 14) {
        flawless[level] = status;
    }
}

void Account::loadFile()
{
    std::string userFolder = Folders::getUserSavePath();
    std::vector<std::string> userFiles = list_directory(userFolder);

    int mostRecentAccountIndex = -1;
    time_t mostRecentTimestamp = 0;

    for (const auto& userFile : userFiles) {
        if (userFile.find(".json") != std::string::npos) {
            std::string userFilePath = userFolder + "/" + userFile;
            std::ifstream file(userFilePath, std::ifstream::binary);
            if (file.is_open()) {
                Json::Value accountJson;
                file >> accountJson;

                // Create new account from JSON
                Account newAccount(accountJson["name"].asString());
                newAccount.difficulty = accountJson["difficulty"].asInt();
                newAccount.progress = accountJson["progress"].asInt();
                newAccount.points = accountJson["points"].asFloat();

                // Load campaign progress
                for (const auto& campaignName : accountJson["campaignProgress"].getMemberNames()) {
                    Json::Value progressJson = accountJson["campaignProgress"][campaignName];
                    CampaignProgress progress;
                    progress.score = progressJson["score"].asInt();
                    progress.highscore = progressJson["highscore"].asInt();
                    progress.fasttime = progressJson["fasttime"].asFloat();
                    progress.time = progressJson["time"].asFloat();
                    for (const auto& choice : progressJson["choices"]) {
                        progress.choices.push_back(choice.asInt());
                    }
                    newAccount.campaignProgress[campaignName] = progress;
                }

                // Load highscores and fast times
                for (int i = 0; i < 50; ++i) {
                    newAccount.highscore[i] = accountJson["highscore"][i].asInt();
                    newAccount.fasttime[i] = accountJson["fasttime"][i].asFloat();
                }

                // Load unlocked levels
                for (int i = 0; i < 60; ++i) {
                    newAccount.unlocked[i] = accountJson["unlocked"][i].asBool();
                }

                // Load the flawless data
                for (int i = 0; i < 14; i++) {
                    newAccount.flawless[i] = accountJson["flawless"][i].asBool();
                }

                accounts.push_back(newAccount);
                file.close();

                // Track most recent account
                struct stat fileInfo;
                stat(userFilePath.c_str(), &fileInfo);
                if (fileInfo.st_mtime > mostRecentTimestamp) {
                    mostRecentTimestamp = fileInfo.st_mtime;
                    mostRecentAccountIndex = accounts.size() - 1;
                }
            }
        }
    }

    if (mostRecentAccountIndex != -1) {
        setActive(mostRecentAccountIndex);
    } else {
        i_active = -1;
    }
}


void Account::saveFile(const std::string& username)
{
    Json::Value accountJson;

    // Populate the JSON object with account details
    accountJson["name"] = name;
    accountJson["difficulty"] = difficulty;
    accountJson["progress"] = progress;
    accountJson["points"] = points;

    // Campaign progress
    for (const auto& [campaignName, progress] : campaignProgress) {
        Json::Value progressJson;
        progressJson["score"] = progress.score;
        progressJson["highscore"] = progress.highscore;
        progressJson["fasttime"] = progress.fasttime;
        progressJson["time"] = progress.time;

        for (int choice : progress.choices) {
            progressJson["choices"].append(choice);
        }
        accountJson["campaignProgress"][campaignName] = progressJson;
    }

    // Highscores and fast times
    for (int i = 0; i < 50; ++i) {
        accountJson["highscore"].append(highscore[i]);
        accountJson["fasttime"].append(fasttime[i]);
    }

    // Unlocked levels
    for (int i = 0; i < 60; ++i) {
        accountJson["unlocked"].append(unlocked[i]);
    }

    // Save the flawless data
    for (int i = 0; i < 14; i++) {
        accountJson["flawless"].append(flawless[i]);
    }

    // Save JSON to file
    std::string userFilePath = Folders::getUserSavePath() + "/" + username + ".json";
    std::ofstream file(userFilePath);
    if (file.is_open()) {
        Json::StreamWriterBuilder writer;
        file << Json::writeString(writer, accountJson);
        file.close();
        std::cerr << "Successfully saved account to " << userFilePath << std::endl;
    } else {
        std::cerr << "Failed to save account: " << username << std::endl;
    }
}


void Account::saveActive()
{
    if (i_active >= 0 && i_active < accounts.size()) {
        // Call the instance method saveFile on the active account
        accounts[i_active].saveFile(accounts[i_active].getName());
    } else {
        cerr << "Unable to find active account" << endl;
    }
}

void Account::loadOldFile(const std::string& filename)
{
    FILE* tfile;
    int numaccounts;
    int iactive;
    errno = 0;

    tfile = fopen(filename.c_str(), "rb");

    if (tfile) {
        // Unpack number of accounts
        funpackf(tfile, "Bi", &numaccounts);
        funpackf(tfile, "Bi", &iactive);
        printf("Loading %d accounts\n", numaccounts);
        for (int i = 0; i < numaccounts; i++) {
            printf("Loading account %d/%d\n", i + 1, numaccounts);
            // Create an account from the binary data and add it to memory
            accounts.emplace_back(tfile);
        }

        fclose(tfile);
        setActive(iactive);  // Set the active account
    } else {
        perror(("Couldn't load users from " + filename).c_str());
        i_active = -1;
    }
}

void Account::convertOldUsersFile(const std::string& oldFilePath)
{
    // Load accounts using the old binary format
    loadOldFile(oldFilePath);

    // Convert each account in memory to JSON and save
    for (size_t i = 0; i < accounts.size(); i++) {
        std::string username = accounts[i].getName();
        if (!username.empty()) {
            // Save each account as a JSON file
            accounts[i].saveFile(username);
        } else {
            std::cerr << "Skipping account with empty name (index " << i << ")" << std::endl;
        }
    }

    std::cout << "All old accounts have been converted to JSON." << std::endl;

    accounts.clear();
    i_active = -1; // Reset active account index
}